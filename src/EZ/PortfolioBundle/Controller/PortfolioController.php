<?php

namespace EZ\PortfolioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PortfolioController extends Controller {

    public function indexAction() {
        return $this->render('EZPortfolioBundle:Portfolio:index.html.twig');
    }

    public function searchProjectAction($src) {

        switch($src) {
            case 'scplateform':
                $url = array('name' => 'SC Plateform', 'src' => 'http://elielaloum.fr/prestashop');
                break;

            case 'customlocalhost':
                $url = array('name' => 'Localhost', 'src' => 'http://elielaloum.fr/prestashop');
                break;

            case 'ppe1':
                $url = array('name' => 'PPE1', 'src' => 'http://elielaloum.fr/prestashop');
                break;

            case 'ppe2':
                $url = array('name' => 'PPE2', 'src' => 'http://elielaloum.fr/prestashop');
                break;

            case 'guessWhat':
                $url = array('name' => 'GuessWhats', 'src' => 'http://elielaloum.fr/projects/prestashop');
                break;

            case 'igaboho':
                $url = array('name' => 'Iga Boho', 'src' => 'http://elielaloum.fr/prestashop');
                break;

            default :
                $url = array('name' => '', 'src' => '');
                break;

        }
        return $this->render('EZPortfolioBundle:Portfolio:portfolio_view.html.twig', $url);
    }

}