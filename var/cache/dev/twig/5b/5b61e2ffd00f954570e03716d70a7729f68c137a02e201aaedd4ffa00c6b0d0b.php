<?php

/* CoreSphereConsoleBundle:Toolbar:toolbar.html.twig */
class __TwigTemplate_b5359e3de0fc325f5412547b7bd6a3a09288c4f3ebf098ed68802de8eb56886c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebProfilerBundle:Profiler:layout.html.twig", "CoreSphereConsoleBundle:Toolbar:toolbar.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebProfilerBundle:Profiler:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5501bc1f9bfe508314adbe9e4804c742c5ec38fa6ae85cbc08449e450cfb95be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5501bc1f9bfe508314adbe9e4804c742c5ec38fa6ae85cbc08449e450cfb95be->enter($__internal_5501bc1f9bfe508314adbe9e4804c742c5ec38fa6ae85cbc08449e450cfb95be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreSphereConsoleBundle:Toolbar:toolbar.html.twig"));

        $__internal_8e43bc80f5d69430d494d4f8271ef2561a396873f1f87063b07013d8f752c238 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e43bc80f5d69430d494d4f8271ef2561a396873f1f87063b07013d8f752c238->enter($__internal_8e43bc80f5d69430d494d4f8271ef2561a396873f1f87063b07013d8f752c238_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreSphereConsoleBundle:Toolbar:toolbar.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5501bc1f9bfe508314adbe9e4804c742c5ec38fa6ae85cbc08449e450cfb95be->leave($__internal_5501bc1f9bfe508314adbe9e4804c742c5ec38fa6ae85cbc08449e450cfb95be_prof);

        
        $__internal_8e43bc80f5d69430d494d4f8271ef2561a396873f1f87063b07013d8f752c238->leave($__internal_8e43bc80f5d69430d494d4f8271ef2561a396873f1f87063b07013d8f752c238_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_9089b20fbafc9ab36f59e3ef0677516f26dd7c4bd8ebc07e2edb64a2a5306b0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9089b20fbafc9ab36f59e3ef0677516f26dd7c4bd8ebc07e2edb64a2a5306b0d->enter($__internal_9089b20fbafc9ab36f59e3ef0677516f26dd7c4bd8ebc07e2edb64a2a5306b0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_1775cf4928c22bf970efb8fe5ef91c031256bfc391244962c7fd91237857a4e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1775cf4928c22bf970efb8fe5ef91c031256bfc391244962c7fd91237857a4e9->enter($__internal_1775cf4928c22bf970efb8fe5ef91c031256bfc391244962c7fd91237857a4e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("console");
        echo "\" class=\"coresphere_console_popover\">
            <img width=\"13\" height=\"28\" alt=\"Console\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAcCAYAAABh2p9gAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ
bWFnZVJlYWR5ccllPAAAAG5JREFUeNpi/P//PwM1ARMDlcGogZQDlhMnTlAtmi0sLBip70IkNiOF
rsMwkKGiouL/CE42Bw4cAGOqGejg4AA3mGouRDaYnGSD1YXoFhCyhIVYm4l16UguvmB5keLCAVTk
UNOFjKO13ggwECDAAAMNHZ7ErsJjAAAAAElFTkSuQmCC\"/>
        </a>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 12
        echo "    ";
        $context["text"] = ('' === $tmp = "Console") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 13
        echo "
    ";
        // line 14
        $this->loadTemplate("WebProfilerBundle:Profiler:toolbar_item.html.twig", "CoreSphereConsoleBundle:Toolbar:toolbar.html.twig", 14)->display(array_merge($context, array("link" => false)));
        
        $__internal_1775cf4928c22bf970efb8fe5ef91c031256bfc391244962c7fd91237857a4e9->leave($__internal_1775cf4928c22bf970efb8fe5ef91c031256bfc391244962c7fd91237857a4e9_prof);

        
        $__internal_9089b20fbafc9ab36f59e3ef0677516f26dd7c4bd8ebc07e2edb64a2a5306b0d->leave($__internal_9089b20fbafc9ab36f59e3ef0677516f26dd7c4bd8ebc07e2edb64a2a5306b0d_prof);

    }

    public function getTemplateName()
    {
        return "CoreSphereConsoleBundle:Toolbar:toolbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 14,  66 => 13,  63 => 12,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'WebProfilerBundle:Profiler:layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        <a href=\"{{ path('console') }}\" class=\"coresphere_console_popover\">
            <img width=\"13\" height=\"28\" alt=\"Console\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAcCAYAAABh2p9gAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJ
bWFnZVJlYWR5ccllPAAAAG5JREFUeNpi/P//PwM1ARMDlcGogZQDlhMnTlAtmi0sLBip70IkNiOF
rsMwkKGiouL/CE42Bw4cAGOqGejg4AA3mGouRDaYnGSD1YXoFhCyhIVYm4l16UguvmB5keLCAVTk
UNOFjKO13ggwECDAAAMNHZ7ErsJjAAAAAElFTkSuQmCC\"/>
        </a>
    {% endset %}
    {% set text %}Console{% endset %}

    {% include 'WebProfilerBundle:Profiler:toolbar_item.html.twig' with { 'link': false } %}
{% endblock %}
", "CoreSphereConsoleBundle:Toolbar:toolbar.html.twig", "C:\\wamp64\\www\\personal\\EZone\\vendor\\coresphere\\console-bundle/Resources/views/Toolbar/toolbar.html.twig");
    }
}
