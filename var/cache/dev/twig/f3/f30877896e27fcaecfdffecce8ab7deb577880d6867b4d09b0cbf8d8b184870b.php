<?php

/* CoreSphereConsoleBundle::base.html.twig */
class __TwigTemplate_2bb24bac879506fb3f3f62626c1c5ebdd034763519aa4f6fce1f4e455dfd5768 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7959b809f192e39ab1f3f922d32f86ebb4aa840885d63ca75bdea0d0f3eecbc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7959b809f192e39ab1f3f922d32f86ebb4aa840885d63ca75bdea0d0f3eecbc5->enter($__internal_7959b809f192e39ab1f3f922d32f86ebb4aa840885d63ca75bdea0d0f3eecbc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreSphereConsoleBundle::base.html.twig"));

        $__internal_6df3e230bbf44cf4c1789f2bb940e7790d3842fd37f8200810826f901e8626cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6df3e230bbf44cf4c1789f2bb940e7790d3842fd37f8200810826f901e8626cf->enter($__internal_6df3e230bbf44cf4c1789f2bb940e7790d3842fd37f8200810826f901e8626cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreSphereConsoleBundle::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 12
        $this->displayBlock('body', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "    </body>
</html>
";
        
        $__internal_7959b809f192e39ab1f3f922d32f86ebb4aa840885d63ca75bdea0d0f3eecbc5->leave($__internal_7959b809f192e39ab1f3f922d32f86ebb4aa840885d63ca75bdea0d0f3eecbc5_prof);

        
        $__internal_6df3e230bbf44cf4c1789f2bb940e7790d3842fd37f8200810826f901e8626cf->leave($__internal_6df3e230bbf44cf4c1789f2bb940e7790d3842fd37f8200810826f901e8626cf_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_757c4b40a2d4fbf8365695023f855e56a05cda4b6c3685d3730e18f92fa50529 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_757c4b40a2d4fbf8365695023f855e56a05cda4b6c3685d3730e18f92fa50529->enter($__internal_757c4b40a2d4fbf8365695023f855e56a05cda4b6c3685d3730e18f92fa50529_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b9408181c76d4310150badd5d71fc1575a4827298cfcfd52e7d3fba3e98bc353 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9408181c76d4310150badd5d71fc1575a4827298cfcfd52e7d3fba3e98bc353->enter($__internal_b9408181c76d4310150badd5d71fc1575a4827298cfcfd52e7d3fba3e98bc353_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "CoreSphere Console";
        
        $__internal_b9408181c76d4310150badd5d71fc1575a4827298cfcfd52e7d3fba3e98bc353->leave($__internal_b9408181c76d4310150badd5d71fc1575a4827298cfcfd52e7d3fba3e98bc353_prof);

        
        $__internal_757c4b40a2d4fbf8365695023f855e56a05cda4b6c3685d3730e18f92fa50529->leave($__internal_757c4b40a2d4fbf8365695023f855e56a05cda4b6c3685d3730e18f92fa50529_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_995b6a2547b2863e8b23ecd8ceb4517486aef6b583912cd3771acb0652366f60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_995b6a2547b2863e8b23ecd8ceb4517486aef6b583912cd3771acb0652366f60->enter($__internal_995b6a2547b2863e8b23ecd8ceb4517486aef6b583912cd3771acb0652366f60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_7098dc16a1d25750b77c69f4158da152843b076f7f99b7fcbbb37d6096202651 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7098dc16a1d25750b77c69f4158da152843b076f7f99b7fcbbb37d6096202651->enter($__internal_7098dc16a1d25750b77c69f4158da152843b076f7f99b7fcbbb37d6096202651_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/coresphereconsole/css/base.css"), "html", null, true);
        echo "\" type=\"text/css\" />
        ";
        
        $__internal_7098dc16a1d25750b77c69f4158da152843b076f7f99b7fcbbb37d6096202651->leave($__internal_7098dc16a1d25750b77c69f4158da152843b076f7f99b7fcbbb37d6096202651_prof);

        
        $__internal_995b6a2547b2863e8b23ecd8ceb4517486aef6b583912cd3771acb0652366f60->leave($__internal_995b6a2547b2863e8b23ecd8ceb4517486aef6b583912cd3771acb0652366f60_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_b05ec53459cb83dbec588b773a00ed32dd315b2c44c73c9c9ea064a8ad70e1c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b05ec53459cb83dbec588b773a00ed32dd315b2c44c73c9c9ea064a8ad70e1c1->enter($__internal_b05ec53459cb83dbec588b773a00ed32dd315b2c44c73c9c9ea064a8ad70e1c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f9d83313ca54b694f574879fddfc8e8855f1525ee07629629929180b2926bdaa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9d83313ca54b694f574879fddfc8e8855f1525ee07629629929180b2926bdaa->enter($__internal_f9d83313ca54b694f574879fddfc8e8855f1525ee07629629929180b2926bdaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        echo "";
        
        $__internal_f9d83313ca54b694f574879fddfc8e8855f1525ee07629629929180b2926bdaa->leave($__internal_f9d83313ca54b694f574879fddfc8e8855f1525ee07629629929180b2926bdaa_prof);

        
        $__internal_b05ec53459cb83dbec588b773a00ed32dd315b2c44c73c9c9ea064a8ad70e1c1->leave($__internal_b05ec53459cb83dbec588b773a00ed32dd315b2c44c73c9c9ea064a8ad70e1c1_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b494d2915ae7dbc9b0833d0629924af4e0ffba354859879b1a6c794cf69c832f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b494d2915ae7dbc9b0833d0629924af4e0ffba354859879b1a6c794cf69c832f->enter($__internal_b494d2915ae7dbc9b0833d0629924af4e0ffba354859879b1a6c794cf69c832f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_964b9f5fa6020aeeb1e0a80f33f16ff67c9ccb18f5bf68ad23d91e9fa1e56f4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_964b9f5fa6020aeeb1e0a80f33f16ff67c9ccb18f5bf68ad23d91e9fa1e56f4a->enter($__internal_964b9f5fa6020aeeb1e0a80f33f16ff67c9ccb18f5bf68ad23d91e9fa1e56f4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 14
        echo "            ";
        // line 15
        echo "            <script>
            window.jQuery || document.write('<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js\"><\\/script>')
            </script>
            <script>
            window.jQuery || document.write(\"<script src=\\\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/coresphereconsole/js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\\\"><\\/script>\");
            </script>
        ";
        
        $__internal_964b9f5fa6020aeeb1e0a80f33f16ff67c9ccb18f5bf68ad23d91e9fa1e56f4a->leave($__internal_964b9f5fa6020aeeb1e0a80f33f16ff67c9ccb18f5bf68ad23d91e9fa1e56f4a_prof);

        
        $__internal_b494d2915ae7dbc9b0833d0629924af4e0ffba354859879b1a6c794cf69c832f->leave($__internal_b494d2915ae7dbc9b0833d0629924af4e0ffba354859879b1a6c794cf69c832f_prof);

    }

    public function getTemplateName()
    {
        return "CoreSphereConsoleBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 19,  134 => 15,  132 => 14,  123 => 13,  105 => 12,  92 => 7,  83 => 6,  65 => 5,  53 => 22,  50 => 13,  48 => 12,  41 => 9,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title 'CoreSphere Console' %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/coresphereconsole/css/base.css') }}\" type=\"text/css\" />
        {% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body '' %}
        {% block javascripts %}
            {# Load jQuery from Google CDN with a local fallback when not laded yet #}
            <script>
            window.jQuery || document.write('<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js\"><\\/script>')
            </script>
            <script>
            window.jQuery || document.write(\"<script src=\\\"{{ asset('bundles/coresphereconsole/js/jquery-1.8.3.min.js') }}\\\"><\\/script>\");
            </script>
        {% endblock %}
    </body>
</html>
", "CoreSphereConsoleBundle::base.html.twig", "C:\\wamp64\\www\\personal\\EZone\\vendor\\coresphere\\console-bundle/Resources/views/base.html.twig");
    }
}
