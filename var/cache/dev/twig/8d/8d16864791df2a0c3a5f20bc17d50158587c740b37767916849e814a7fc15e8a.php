<?php

/* EZPortfolioBundle::Layout/layout.html.twig */
class __TwigTemplate_97a39cdda36de5d496bf5ebda28f9e05efc86fd39a94734e3f27df79efa7a088 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::template.html.twig", "EZPortfolioBundle::Layout/layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'ezhome_body' => array($this, 'block_ezhome_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b93cc90023eefe0df47999a091f633007beeb2271f6954bc06040b486b77aa6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b93cc90023eefe0df47999a091f633007beeb2271f6954bc06040b486b77aa6->enter($__internal_0b93cc90023eefe0df47999a091f633007beeb2271f6954bc06040b486b77aa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle::Layout/layout.html.twig"));

        $__internal_d2b8cdb53d8c3817c2d23fe84ce4a106f96b969c2e5945c4d29c4336164cbd30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2b8cdb53d8c3817c2d23fe84ce4a106f96b969c2e5945c4d29c4336164cbd30->enter($__internal_d2b8cdb53d8c3817c2d23fe84ce4a106f96b969c2e5945c4d29c4336164cbd30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle::Layout/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b93cc90023eefe0df47999a091f633007beeb2271f6954bc06040b486b77aa6->leave($__internal_0b93cc90023eefe0df47999a091f633007beeb2271f6954bc06040b486b77aa6_prof);

        
        $__internal_d2b8cdb53d8c3817c2d23fe84ce4a106f96b969c2e5945c4d29c4336164cbd30->leave($__internal_d2b8cdb53d8c3817c2d23fe84ce4a106f96b969c2e5945c4d29c4336164cbd30_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ea5a9f762183d8740a4ef8e448b0223fe3381c4e5758027ecf42ab5bcc216980 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea5a9f762183d8740a4ef8e448b0223fe3381c4e5758027ecf42ab5bcc216980->enter($__internal_ea5a9f762183d8740a4ef8e448b0223fe3381c4e5758027ecf42ab5bcc216980_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c5164bb0c668efde9a61c72dc08fb384aa7c6417f8b57a19ac1ba43923986ea4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5164bb0c668efde9a61c72dc08fb384aa7c6417f8b57a19ac1ba43923986ea4->enter($__internal_c5164bb0c668efde9a61c72dc08fb384aa7c6417f8b57a19ac1ba43923986ea4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <!-- Header -->
    <header id=\"home\" class=\"intro-header\">
        <div id=\"particles\"></div>
        <div class=\"container\">
            <div class=\"intro-message\">
                <h1>Elie Laloum ,</h1>
                <h3><span class=\"txt-rotate\" data-period=\"2000\" data-rotate='[ \"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("I am a developer."), "html", null, true);
        echo "\", \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("I am a WebDesigner."), "html", null, true);
        echo "\", \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Welcome!."), "html", null, true);
        echo "\" ]'></span></h3>
                <hr class=\"intro-divider\">
                <div class=\"container\">
                    <div class=\"row justify-content-center intro-button\">
                        <div class=\"col-lg-4\">
                            <a href=\"#contact\" class=\"btn btn-secondary btn-lg\">
                                <span class=\"network-name\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("HIRE ME"), "html", null, true);
        echo "</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row justify-content-center intro-button\">
                <div class=\"mouse mt-auto mb-2 text-center\">
                   <img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/img/mouse.gif"), "html", null, true);
        echo "\" style=\"height: 100px; width: 100px;\">
                </div>
            </div>
        </div>
    </header>

    ";
        // line 31
        $this->displayBlock('ezhome_body', $context, $blocks);
        // line 34
        echo "

";
        
        $__internal_c5164bb0c668efde9a61c72dc08fb384aa7c6417f8b57a19ac1ba43923986ea4->leave($__internal_c5164bb0c668efde9a61c72dc08fb384aa7c6417f8b57a19ac1ba43923986ea4_prof);

        
        $__internal_ea5a9f762183d8740a4ef8e448b0223fe3381c4e5758027ecf42ab5bcc216980->leave($__internal_ea5a9f762183d8740a4ef8e448b0223fe3381c4e5758027ecf42ab5bcc216980_prof);

    }

    // line 31
    public function block_ezhome_body($context, array $blocks = array())
    {
        $__internal_8ee5858f49bbb54da1ba236ac6d440cdff522fed37f2e216414774f41c2e44b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ee5858f49bbb54da1ba236ac6d440cdff522fed37f2e216414774f41c2e44b2->enter($__internal_8ee5858f49bbb54da1ba236ac6d440cdff522fed37f2e216414774f41c2e44b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezhome_body"));

        $__internal_6df3ee33469ca3944d3c044cf0ed40c8fed49aa5bd32d98716a58ef540b58734 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6df3ee33469ca3944d3c044cf0ed40c8fed49aa5bd32d98716a58ef540b58734->enter($__internal_6df3ee33469ca3944d3c044cf0ed40c8fed49aa5bd32d98716a58ef540b58734_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezhome_body"));

        // line 32
        echo "
    ";
        
        $__internal_6df3ee33469ca3944d3c044cf0ed40c8fed49aa5bd32d98716a58ef540b58734->leave($__internal_6df3ee33469ca3944d3c044cf0ed40c8fed49aa5bd32d98716a58ef540b58734_prof);

        
        $__internal_8ee5858f49bbb54da1ba236ac6d440cdff522fed37f2e216414774f41c2e44b2->leave($__internal_8ee5858f49bbb54da1ba236ac6d440cdff522fed37f2e216414774f41c2e44b2_prof);

    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle::Layout/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 32,  106 => 31,  94 => 34,  92 => 31,  83 => 25,  71 => 16,  58 => 10,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::template.html.twig\" %}

{% block body %}
    <!-- Header -->
    <header id=\"home\" class=\"intro-header\">
        <div id=\"particles\"></div>
        <div class=\"container\">
            <div class=\"intro-message\">
                <h1>Elie Laloum ,</h1>
                <h3><span class=\"txt-rotate\" data-period=\"2000\" data-rotate='[ \"{{ 'I am a developer.'|trans }}\", \"{{ 'I am a WebDesigner.'|trans }}\", \"{{ 'Welcome!.'|trans }}\" ]'></span></h3>
                <hr class=\"intro-divider\">
                <div class=\"container\">
                    <div class=\"row justify-content-center intro-button\">
                        <div class=\"col-lg-4\">
                            <a href=\"#contact\" class=\"btn btn-secondary btn-lg\">
                                <span class=\"network-name\">{{ 'HIRE ME'|trans }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row justify-content-center intro-button\">
                <div class=\"mouse mt-auto mb-2 text-center\">
                   <img src=\"{{ asset('bundles/webshome/img/mouse.gif') }}\" style=\"height: 100px; width: 100px;\">
                </div>
            </div>
        </div>
    </header>

    {% block ezhome_body %}

    {% endblock %}


{% endblock %}", "EZPortfolioBundle::Layout/layout.html.twig", "C:\\wamp64\\www\\personal\\EZone\\src\\EZ\\PortfolioBundle/Resources/views/Layout/layout.html.twig");
    }
}
