<?php

/* ::template.html.twig */
class __TwigTemplate_e25fc436da6b98c92c8bb237f3c3bd217c089f836167aa2c028ed3ac174402ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f2f3ad5663f278a6feaf69cae07ed53ff6959a602fa33902f5832455e926bb9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f2f3ad5663f278a6feaf69cae07ed53ff6959a602fa33902f5832455e926bb9->enter($__internal_4f2f3ad5663f278a6feaf69cae07ed53ff6959a602fa33902f5832455e926bb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::template.html.twig"));

        $__internal_4a1a03bf661f13c6cbcdbade36c22f9f453beea09f81eaa842d240fac4ac0945 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a1a03bf661f13c6cbcdbade36c22f9f453beea09f81eaa842d240fac4ac0945->enter($__internal_4a1a03bf661f13c6cbcdbade36c22f9f453beea09f81eaa842d240fac4ac0945_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::template.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/html\">

    <head>

        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        ";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        // line 12
        echo "
        ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 23
        echo "
    </head>

        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#home\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("HOME"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ABOUT"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#services\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SERVICES"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#works\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("WORKS"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#contact\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("CONTACT"), "html", null, true);
        echo "</a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mr-0\">
                        <li class=\"nav-item\">
                            <a href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_homepage", array("_locale" => "fr"));
        echo "\" class=\"nav-link\"><img class=\"mt-1\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/img/fr16.png"), "html", null, true);
        echo "\"></a>
                        </li>
                        <li class=\"nav-item mr-0\">
                            <a href=\"";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_homepage", array("_locale" => "en"));
        echo "\" class=\"nav-link\"><img class=\"mt-1\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/img/gb16.png"), "html", null, true);
        echo "\"></a>
                        </li>
                        <li class=\"nav-item mr-5\">
                            <a href=\"\" class=\"nav-link\"><img class=\"mt-1\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/img/lock.png"), "html", null, true);
        echo "\"></a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        ";
        // line 66
        $this->displayBlock('body', $context, $blocks);
        // line 69
        echo "
        ";
        // line 70
        $this->displayBlock('javascripts', $context, $blocks);
        // line 83
        echo "
    </body>

</html>";
        
        $__internal_4f2f3ad5663f278a6feaf69cae07ed53ff6959a602fa33902f5832455e926bb9->leave($__internal_4f2f3ad5663f278a6feaf69cae07ed53ff6959a602fa33902f5832455e926bb9_prof);

        
        $__internal_4a1a03bf661f13c6cbcdbade36c22f9f453beea09f81eaa842d240fac4ac0945->leave($__internal_4a1a03bf661f13c6cbcdbade36c22f9f453beea09f81eaa842d240fac4ac0945_prof);

    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        $__internal_d60aa74e3e2186189b86de5f21af4eb8cbe221a2157eaa8d11e779875bcd3f1b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d60aa74e3e2186189b86de5f21af4eb8cbe221a2157eaa8d11e779875bcd3f1b->enter($__internal_d60aa74e3e2186189b86de5f21af4eb8cbe221a2157eaa8d11e779875bcd3f1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d51da797387c43cd4566570a63e2ce4b7839c2cc67015b9f730519678bda4540 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d51da797387c43cd4566570a63e2ce4b7839c2cc67015b9f730519678bda4540->enter($__internal_d51da797387c43cd4566570a63e2ce4b7839c2cc67015b9f730519678bda4540_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 10
        echo "            Portfolio
        ";
        
        $__internal_d51da797387c43cd4566570a63e2ce4b7839c2cc67015b9f730519678bda4540->leave($__internal_d51da797387c43cd4566570a63e2ce4b7839c2cc67015b9f730519678bda4540_prof);

        
        $__internal_d60aa74e3e2186189b86de5f21af4eb8cbe221a2157eaa8d11e779875bcd3f1b->leave($__internal_d60aa74e3e2186189b86de5f21af4eb8cbe221a2157eaa8d11e779875bcd3f1b_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9ba4d4ca0aba2d6e338309e0ea8e777ff76ea8a465628bba50fa888239cc992b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ba4d4ca0aba2d6e338309e0ea8e777ff76ea8a465628bba50fa888239cc992b->enter($__internal_9ba4d4ca0aba2d6e338309e0ea8e777ff76ea8a465628bba50fa888239cc992b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_8e510d7f885137c5905b54967d9feacd9516d1f7631415c212d5edd21790ead2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e510d7f885137c5905b54967d9feacd9516d1f7631415c212d5edd21790ead2->enter($__internal_8e510d7f885137c5905b54967d9feacd9516d1f7631415c212d5edd21790ead2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "
            <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">

            <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/css/landing-page.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/css/jquery.mCustomScrollbar.css"), "html", null, true);
        echo "\" type=\"text/css\" />

        ";
        
        $__internal_8e510d7f885137c5905b54967d9feacd9516d1f7631415c212d5edd21790ead2->leave($__internal_8e510d7f885137c5905b54967d9feacd9516d1f7631415c212d5edd21790ead2_prof);

        
        $__internal_9ba4d4ca0aba2d6e338309e0ea8e777ff76ea8a465628bba50fa888239cc992b->leave($__internal_9ba4d4ca0aba2d6e338309e0ea8e777ff76ea8a465628bba50fa888239cc992b_prof);

    }

    // line 66
    public function block_body($context, array $blocks = array())
    {
        $__internal_b655909a280c9b6a94f497d9f93215f43bcdba6f24a2c978017b4a5d228cb354 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b655909a280c9b6a94f497d9f93215f43bcdba6f24a2c978017b4a5d228cb354->enter($__internal_b655909a280c9b6a94f497d9f93215f43bcdba6f24a2c978017b4a5d228cb354_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0b64722517ccab0f9e5223abe812338d0cd009e3fb38e2f0b47c09cb954a5a64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b64722517ccab0f9e5223abe812338d0cd009e3fb38e2f0b47c09cb954a5a64->enter($__internal_0b64722517ccab0f9e5223abe812338d0cd009e3fb38e2f0b47c09cb954a5a64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 67
        echo "
        ";
        
        $__internal_0b64722517ccab0f9e5223abe812338d0cd009e3fb38e2f0b47c09cb954a5a64->leave($__internal_0b64722517ccab0f9e5223abe812338d0cd009e3fb38e2f0b47c09cb954a5a64_prof);

        
        $__internal_b655909a280c9b6a94f497d9f93215f43bcdba6f24a2c978017b4a5d228cb354->leave($__internal_b655909a280c9b6a94f497d9f93215f43bcdba6f24a2c978017b4a5d228cb354_prof);

    }

    // line 70
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_783fafebb5209db0c0831af9567cb33f5112b43a7c07a2fd4463cd18db70e308 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_783fafebb5209db0c0831af9567cb33f5112b43a7c07a2fd4463cd18db70e308->enter($__internal_783fafebb5209db0c0831af9567cb33f5112b43a7c07a2fd4463cd18db70e308_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_38d5f7958511e144c458d712e2a2254fb580127ba8733213ca52ee32cc415e77 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38d5f7958511e144c458d712e2a2254fb580127ba8733213ca52ee32cc415e77->enter($__internal_38d5f7958511e144c458d712e2a2254fb580127ba8733213ca52ee32cc415e77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 71
        echo "
            <!-- Script -->
            <script src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/popper/popper.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/particle.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/skillanim.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/main.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/txtRotate.js"), "html", null, true);
        echo "\"></script>

        ";
        
        $__internal_38d5f7958511e144c458d712e2a2254fb580127ba8733213ca52ee32cc415e77->leave($__internal_38d5f7958511e144c458d712e2a2254fb580127ba8733213ca52ee32cc415e77_prof);

        
        $__internal_783fafebb5209db0c0831af9567cb33f5112b43a7c07a2fd4463cd18db70e308->leave($__internal_783fafebb5209db0c0831af9567cb33f5112b43a7c07a2fd4463cd18db70e308_prof);

    }

    public function getTemplateName()
    {
        return "::template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 80,  255 => 79,  251 => 78,  247 => 77,  243 => 76,  239 => 75,  235 => 74,  231 => 73,  227 => 71,  218 => 70,  207 => 67,  198 => 66,  185 => 20,  181 => 19,  177 => 18,  173 => 17,  168 => 14,  159 => 13,  148 => 10,  139 => 9,  126 => 83,  124 => 70,  121 => 69,  119 => 66,  108 => 58,  100 => 55,  92 => 52,  84 => 47,  78 => 44,  72 => 41,  66 => 38,  60 => 35,  46 => 23,  44 => 13,  41 => 12,  39 => 9,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/html\">

    <head>

        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        {% block title %}
            Portfolio
        {% endblock %}

        {% block stylesheets %}

            <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">

            <link href=\"{{ asset('bundles/webshome/css/landing-page.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"{{ asset('bundles/webshome/vendor/font-awesome/css/font-awesome.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"{{ asset('bundles/webshome/vendor/bootstrap/css/bootstrap.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/webshome/vendor/bootstrap/css/jquery.mCustomScrollbar.css') }}\" type=\"text/css\" />

        {% endblock %}

    </head>

        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#home\">{{ 'HOME'|trans }}</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\">{{ 'ABOUT'|trans }}</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#services\">{{ 'SERVICES'|trans }}</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#works\">{{ 'WORKS'|trans }}</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#contact\">{{ 'CONTACT'|trans }}</a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mr-0\">
                        <li class=\"nav-item\">
                            <a href=\"{{ path('ez_portfolio_homepage', {'_locale': 'fr'}) }}\" class=\"nav-link\"><img class=\"mt-1\" src=\"{{ asset('bundles/webshome/img/fr16.png') }}\"></a>
                        </li>
                        <li class=\"nav-item mr-0\">
                            <a href=\"{{ path('ez_portfolio_homepage', {'_locale': 'en'}) }}\" class=\"nav-link\"><img class=\"mt-1\" src=\"{{ asset('bundles/webshome/img/gb16.png') }}\"></a>
                        </li>
                        <li class=\"nav-item mr-5\">
                            <a href=\"\" class=\"nav-link\"><img class=\"mt-1\" src=\"{{ asset('bundles/webshome/img/lock.png') }}\"></a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        {% block body %}

        {% endblock %}

        {% block javascripts %}

            <!-- Script -->
            <script src=\"{{ asset('bundles/webshome/vendor/jquery/jquery.min.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/vendor/popper/popper.min.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/vendor/bootstrap/js/bootstrap.min.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/js/particle.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/js/skillanim.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/js/main.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/js/jquery.mCustomScrollbar.concat.min.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/js/txtRotate.js') }}\"></script>

        {% endblock %}

    </body>

</html>", "::template.html.twig", "C:\\wamp64\\www\\personal\\EZone\\app/Resources\\views/template.html.twig");
    }
}
