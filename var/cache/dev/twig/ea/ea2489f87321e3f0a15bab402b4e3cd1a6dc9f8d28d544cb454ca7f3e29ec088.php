<?php

/* EZPortfolioBundle:Portfolio:index.html.twig */
class __TwigTemplate_3ec679ed4e1ada6499f2299585b4eca5386700a0a9734180e47de3f51056cff9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EZPortfolioBundle::Layout/layout.html.twig", "EZPortfolioBundle:Portfolio:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'ezhome_body' => array($this, 'block_ezhome_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EZPortfolioBundle::Layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ade261c182c779153e55eda966e49d7563a9d32a58653ccb656e3af8f31f69e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ade261c182c779153e55eda966e49d7563a9d32a58653ccb656e3af8f31f69e->enter($__internal_9ade261c182c779153e55eda966e49d7563a9d32a58653ccb656e3af8f31f69e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle:Portfolio:index.html.twig"));

        $__internal_c24d7dfc6a3ff6b4b994d96ff33c0d0713b6c572dcac43f0f51ec67830ff19c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c24d7dfc6a3ff6b4b994d96ff33c0d0713b6c572dcac43f0f51ec67830ff19c8->enter($__internal_c24d7dfc6a3ff6b4b994d96ff33c0d0713b6c572dcac43f0f51ec67830ff19c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle:Portfolio:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ade261c182c779153e55eda966e49d7563a9d32a58653ccb656e3af8f31f69e->leave($__internal_9ade261c182c779153e55eda966e49d7563a9d32a58653ccb656e3af8f31f69e_prof);

        
        $__internal_c24d7dfc6a3ff6b4b994d96ff33c0d0713b6c572dcac43f0f51ec67830ff19c8->leave($__internal_c24d7dfc6a3ff6b4b994d96ff33c0d0713b6c572dcac43f0f51ec67830ff19c8_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_824c86e83612b19fb4948da3a28f68a3ff6c1bcd5d12e0303c9174f970ba5dd1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_824c86e83612b19fb4948da3a28f68a3ff6c1bcd5d12e0303c9174f970ba5dd1->enter($__internal_824c86e83612b19fb4948da3a28f68a3ff6c1bcd5d12e0303c9174f970ba5dd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_88124e191f5564fb846fcd4112d4c2b0cd9a39fa08da968d5ac19dbd9030962d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88124e191f5564fb846fcd4112d4c2b0cd9a39fa08da968d5ac19dbd9030962d->enter($__internal_88124e191f5564fb846fcd4112d4c2b0cd9a39fa08da968d5ac19dbd9030962d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "
    <title>
    Home - ";
        // line 6
        $this->displayParentBlock("title", $context, $blocks);
        echo "
    </title>

";
        
        $__internal_88124e191f5564fb846fcd4112d4c2b0cd9a39fa08da968d5ac19dbd9030962d->leave($__internal_88124e191f5564fb846fcd4112d4c2b0cd9a39fa08da968d5ac19dbd9030962d_prof);

        
        $__internal_824c86e83612b19fb4948da3a28f68a3ff6c1bcd5d12e0303c9174f970ba5dd1->leave($__internal_824c86e83612b19fb4948da3a28f68a3ff6c1bcd5d12e0303c9174f970ba5dd1_prof);

    }

    // line 11
    public function block_ezhome_body($context, array $blocks = array())
    {
        $__internal_7f4c773950681406c7114ddaa16f43f6fde6fbe52c1590ed589a614eae6402d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f4c773950681406c7114ddaa16f43f6fde6fbe52c1590ed589a614eae6402d2->enter($__internal_7f4c773950681406c7114ddaa16f43f6fde6fbe52c1590ed589a614eae6402d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezhome_body"));

        $__internal_8a9d3eb7fcc97ef38410fa8c02378448dd07a7847c733b794e74dd60a37d9f7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a9d3eb7fcc97ef38410fa8c02378448dd07a7847c733b794e74dd60a37d9f7c->enter($__internal_8a9d3eb7fcc97ef38410fa8c02378448dd07a7847c733b794e74dd60a37d9f7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezhome_body"));

        // line 12
        echo "
    <!-- Page Content -->
    <section id=\"about\" class=\"content-section-a\">

        <div class=\"container\">
            <div class=\"row\">

                <div class=\"col-lg-5 mr-auto overlayrepere\">
                    <img style=\"height: 90%\" class=\"img-about img-fluid mb-0 mt-auto\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/img/default_avatar.jpg"), "html", null, true);
        echo "\">
                    <div class=\"overlay\">
                        <div class=\"row\">
                            <div class=\"col-lg-12 text-center\">
                                <a href=\"#\" class=\"fa fa-facebook-square fa-3x\"></a>
                                <a href=\"#\" class=\"fa fa-twitter-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-linkedin-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-github-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-google-plus-square  fa-3x ml-1\"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"smallScreenSpace col-lg-5 ml-auto\">
                    <hr class=\"section-heading-spacer\">
                    <div class=\"clearfix\"></div>
                    <h2 class=\"section-heading\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ABOUT ME"), "html", null, true);
        echo "</h2>
                    <p class=\"lead\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Graduate from the A-Level STI2D option SIN and Senior technician patent Computer services to the organizations, I am currently doing a Licence Analyst & Computer Engineering."), "html", null, true);
        echo "<br><br>
                        ";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Passionate about computer science and technology in general I am fascinated by the understanding of the different systems around us."), "html", null, true);
        echo "<br><br>
                        ";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Ambitious and motivated, I am actively looking for an alternative training that will allow me to deploy my skills and talents within a professional infrastructure."), "html", null, true);
        echo "</p>
                </div>
            </div>
        </div>

    </section>

    <section class=\"content-section-c\">

        <div id=\"skills\" class=\"container-fluid\">

            <div class=\"row justify-content-center\">

                <div class=\"col-lg-6 section-skills align-self-center\">

                    <div class=\"container\">

                        <div class=\"row justify-content-center\">

                            <div class=\"col-lg-9\">

                                <h2>";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("MY SKILLS"), "html", null, true);
        echo "</h2><br>
                                <h4>";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SERIOUS SKILLS BUT KEEP LEARNING"), "html", null, true);
        echo "</h4>
                                <p>";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("In 3 years of learning I acquired a lot of knowledge that already allows me to meet the expectations of a project."), "html", null, true);
        echo "</p>

                                <a href=\"#works\" class=\"networklink\">
                                    <span class=\"network-name\">";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SEE MY WORKS"), "html", null, true);
        echo "</span>
                                </a>

                            </div>

                        </div>

                    </div>

                </div>

                <div class=\"col-lg-1\"></div>

                <div class=\"col-lg-4 mr-auto order-lg-2 justify-content-center align-self-center\">

                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("WEB DEV"), "html", null, true);
        echo "</p>
                        <div class=\"skillbar clearfix \" data-percent=\"90%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">90%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("WEBDESIGN"), "html", null, true);
        echo "</p>
                        <div class=\"skillbar clearfix \" data-percent=\"70%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">70%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SOFTWARE DEV"), "html", null, true);
        echo "</p>
                        <div class=\"skillbar clearfix \" data-percent=\"60%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">60%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("APP DEV"), "html", null, true);
        echo "</p>
                        <div class=\"skillbar clearfix \" data-percent=\"50%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">50%</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id=\"services\" class=\"content-section-a text-center\">
        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SERVICES I OFFER"), "html", null, true);
        echo "</h2>

                    <div class=\"row service-row justify-content-center\">

                        <div class=\"col-lg-3 col-sm-6 col-md-6 col-xs-6 mt-4\">

                                <div id=\"st1\" class=\"service-thumb\">

                                    <i class=\"fa fa-wordpress fa-4x\"></i>

                                    <h3>CMS</h3>
                                    <p class=\"lead\">";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("For a project that can be modularized and stylised with fast handling."), "html", null, true);
        echo "</p>
                                </div>

                        </div>

                        <div class=\"col-lg-3 col-sm-6 col-md-6 col-xs-6 mt-4\">

                                <div id=\"st2\" class=\"service-thumb\">

                                    <i class=\"faicon fa fa-paint-brush  fa-4x\"></i>

                                    <h3>WEBDESIGN</h3>
                                    <p class=\"lead\">";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Template styliser according to specifications."), "html", null, true);
        echo "</p>
                                </div>

                        </div>

                        <div class=\"col-lg-3 col-sm-12 col-md-12 col-xs-12 mt-4\">

                                <div id=\"st3\" class=\"service-thumb\">

                                    <i class=\"fa fa-code fa-4x\"></i>

                                    <h3>";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("DEVELOPEMENT"), "html", null, true);
        echo "</h3>
                                    <p class=\"lead\">";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Heavy client development, mobile app or web service."), "html", null, true);
        echo "</p>
                                </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id=\"works\" class=\"content-section-b text-center\">
        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("MY WORKS"), "html", null, true);
        echo "</h2>
                    <br>

                    <div class=\"row justify-content-center\">

                        <div class=\"col-lg-12\">
                            <button class=\"tagBtn\" onclick=\"showTag('personal'); hideTag('school'); hideTag('professional')\">";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Personal"), "html", null, true);
        echo "</button>
                            <button class=\"tagBtn\" onclick=\"showTag('school'); hideTag('personal'); hideTag('professional')\">";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("School"), "html", null, true);
        echo "</button>
                            <button class=\"tagBtn\" onclick=\"showTag('professional'); hideTag('personal'); hideTag('school')\">";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Professional"), "html", null, true);
        echo "</button>
                        </div>

                        <div style=\"\" class=\"col-lg-8 mt-5 personal\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 188
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "scplateform"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">SC Plateform</h5>
                                        <p class=\"col-lg-12\">";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("No describe for now."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 196
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "customlocalhost"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Custom localhost"), "html", null, true);
        echo "</h5>
                                        <p class=\"col-lg-12\">";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("HTML/CSS/JS System for custom localhost ( listing projects , custom links )."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                        <div style=\"display: none;\" class=\"col-lg-8 mt-5 professional\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 212
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "guesswhat"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">GuessWhat</h5>
                                        <p class=\"col-lg-12\">";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Development of a mobile comparator in HTML/CSS/PHP."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 220
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "igaboho"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"";
        // line 221
        echo "http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">Iga Boho Chic</h5>
                                        <p class=\"col-lg-12\">";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Showcase site for an infrastructure of jewelry sales and seasonal clothing."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                        <div style=\"display: none;\" class=\"col-lg-8 mt-5 school\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 236
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "ppe1"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">PPE 1</h5>
                                        <p class=\"col-lg-12\">";
        // line 239
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Diploma Validation Project."), "html", null, true);
        echo "</p>
                                        <p class=\"col-lg-12\">";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Web client for customers of a car rental company."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 245
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "ppe2"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">PPE 2</h5>
                                        <p class=\"col-lg-12\">";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Diploma Validation Project."), "html", null, true);
        echo "</p>
                                        <p class=\"col -lg-12\">";
        // line 249
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Heavy client for the management of the car rental company database."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id=\"experiences\" class=\"content-section-a text-center\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("MY EXPERIENCE"), "html", null, true);
        echo "</h2>

                    <div class=\"row\">

                        <div class=\"frame col-lg-12\">

                            <!-- SMALL SCREEN -->

                            <div class=\"row justify-content-center ExpSmall\">
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-5\">
                                    <div class=\"row\">
                                        <div class=\"col-4 logopart\">
                                            <i class=\"mt-4 fa fa-code fa-3x\" aria-hidden=\"true\"></i>
                                        </div>
                                        <div class=\"descpart col-8 text-left\">
                                            <ul class=\"mt-1\">
                                                <li>GUESSWHAT</li>
                                                <li>2017</li>
                                            </ul>
                                            <p>Mauris balqnuet lorem ipsus dolores minum aerum tanges.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 2</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 3</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 4</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 5</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 6</p>
                                </div>
                            </div>

                            <!-- COMPUTER SCREEN -->

                            <div class=\"row mt-5\">

                                <div class=\"col-lg-12 text-center\">

                                    <div class=\"row\">
                                        <div class=\"col-lg-6 text-left\">
                                            <h3>";
        // line 317
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Professional"), "html", null, true);
        echo "</h3>
                                        </div>
                                        <div class=\"col-lg-6 text-right\">
                                            <h3>";
        // line 320
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Training"), "html", null, true);
        echo "</h3>
                                        </div>
                                    </div>

                                    <div class=\"row mt-4\">

                                        <div class=\"leftexp col-lg-6 mt-3\" style=\"border-left: 4px solid grey\">

                                            <div class=\"row\">
                                                <div class=\"year col-lg-2 mr-0 ml-auto pr-0 pl-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2017</p>
                                                    <hr style=\"border-bottom: 1px dotted grey\">
                                                </div>
                                                <div class=\"thumbexp col-lg-9 pr-0\" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-briefcase fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">";
        // line 339
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("WORK"), "html", null, true);
        echo "</p>
                                                        </div>

                                                        <div class=\"col-lg-8 text-right\" style=\"border-left: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">BNP PARIBAS</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"col\"></div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"year col-lg-2 mr-0 ml-auto pr-0 pl-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2017</p>
                                                    <hr style=\"border-bottom: 1px dotted grey\">
                                                </div>
                                                <div class=\"thumbexp col-lg-9 pr-0\" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-briefcase fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">";
        // line 366
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("TRAINEE"), "html", null, true);
        echo "</p>
                                                        </div>

                                                        <div class=\"col-lg-8 text-right\" style=\"border-left: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">GUESSWHAT</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">";
        // line 371
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Mobile suscription comparator."), "html", null, true);
        echo "
                                                            <br>";
        // line 372
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Integration to another website."), "html", null, true);
        echo "</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"col\"></div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"year col-lg-2 mr-0 ml-auto pr-0 pl-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2016</p>
                                                    <hr style=\"border-bottom: 1px dotted grey\">
                                                </div>
                                                <div class=\"thumbexp col-lg-9 pr-0\" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-briefcase fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">";
        // line 394
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("PROJECT"), "html", null, true);
        echo "</p>
                                                        </div>

                                                        <div class=\"col-lg-8 text-right\" style=\"border-left: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">PPE</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">";
        // line 399
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Web service dev for rent car company."), "html", null, true);
        echo "
                                                            <br>";
        // line 400
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Heavy client to manage db."), "html", null, true);
        echo "
                                                            <br>";
        // line 401
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Mobile app for clients."), "html", null, true);
        echo "</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"col\"></div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"year col-lg-2 mr-0 ml-auto pr-0 pl-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2016</p>
                                                    <hr style=\"border-bottom: 1px dotted grey\">
                                                </div>
                                                <div class=\"thumbexp col-lg-9 pr-0\" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-briefcase fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">";
        // line 423
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("TRAINEE"), "html", null, true);
        echo "</p>
                                                        </div>

                                                        <div class=\"col-lg-8 text-right\" style=\"border-left: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">TECHNISITES</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">";
        // line 428
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Development of a web service for the verification of the state of printers of the computer park."), "html", null, true);
        echo "
                                                            <br>HTML/CSS - PHP/SNMP</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"col\"></div>

                                            </div>

                                        </div>

                                        <div class=\"rightexp col-lg-6 mt-3\" style=\"border-right: 4px solid grey; max-height: 500px;\">

                                            <div class=\"row\">
                                                <div class=\"col\"></div>
                                                <div class=\"thumbexp col-lg-9 \" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col\"></div>

                                                        <div class=\"col-lg-8 text-left\" style=\"border-right: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">CFA INSTA</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">";
        // line 454
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Analyst & Computer Engineering, Development."), "html", null, true);
        echo "</p>
                                                        </div>

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-graduation-cap fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">";
        // line 459
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("BACHELOR"), "html", null, true);
        echo "</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"year col-lg-2 mr-0 ml-auto pl-0 pr-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2018</p>
                                                    <hr>
                                                </div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"col\"></div>
                                                <div class=\"thumbexp col-lg-9 \" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col\"></div>

                                                        <div class=\"col-lg-8 text-left\" style=\"border-right: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">CFA INSTA</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">";
        // line 483
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("IT Service to Organizations."), "html", null, true);
        echo "
                                                             <br>";
        // line 484
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Software solutions and business applications."), "html", null, true);
        echo "</p>
                                                        </div>

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-graduation-cap fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">";
        // line 489
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("12th GR"), "html", null, true);
        echo "</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"year col-lg-2 mr-0 ml-auto pl-0 pr-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2017</p>
                                                    <hr>
                                                </div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"col\"></div>
                                                <div class=\"thumbexp col-lg-9 \" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col\"></div>

                                                        <div class=\"col-lg-8 text-left\" style=\"border-right: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">JEAN PERRIN</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">";
        // line 513
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Science and Technology for Industry and Sustainable Development."), "html", null, true);
        echo "
                                                            <br>";
        // line 514
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Option Science Industrial & Digital."), "html", null, true);
        echo "</p>
                                                        </div>

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-graduation-cap fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">";
        // line 519
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("A-LVL"), "html", null, true);
        echo "</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"year col-lg-2 mr-0 ml-auto pl-0 pr-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2014</p>
                                                    <hr>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </div>
    </section>

    <section id=\"contact\" class=\"content-section-c text-center\">

        <div class=\"container-fluid\">

            <div class=\"row\">

                <div class=\"col-lg-12 section-contact\">
                    <h2 class=\"mt-5\">";
        // line 558
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("CONTACT ME"), "html", null, true);
        echo "</h2>
                    <p>";
        // line 559
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Do not hesitate to contact me if you have any request"), "html", null, true);
        echo "</p>

                    <div class=\"row mb-5\">

                        <div class=\"col-lg-9 mt-5\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8 text-left\">
                                    <h5><strong>";
        // line 566
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Post a message"), "html", null, true);
        echo "</strong></h5>
                                    <form>
                                        <div class=\"form-group\">
                                            <div class=\"mt-4 justify-content-center\">
                                                <input type=\"email\" class=\"emailInput mt-2 col-lg-7 col-10 form-control\" id=\"exampleFormControlInput1\" placeholder=\"";
        // line 570
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Name@example.com"), "html", null, true);
        echo "\">
                                                <input type=\"object\" class=\"objectInput mt-2 col-lg-7 col-10 form-control\" id=\"exampleInputEmail1\" placeholder=\"";
        // line 571
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Enter subject"), "html", null, true);
        echo "\">
                                                <input type=\"tel\" class=\"telInput mt-2 col-lg-7 col-10 form-control\" id=\"exampleInputTel\" placeholder=\"";
        // line 572
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Cellphone number"), "html", null, true);
        echo "\">
                                                <textarea class=\"mt-2 messageInput form-control col-lg-11\" placeholder=\"Message\" id=\"exampleFormControlTextarea1\" rows=\"5\"></textarea>
                                                <button type=\"button\" class=\"mt-2 btn btn-lg col-lg-12 col-12\" style=\"background-color: white; color: gray;\">";
        // line 574
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Send Message"), "html", null, true);
        echo "</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <hr class=\"defdivider\">

                        <div class=\"infoContact col-lg-2 mt-5 text-left mb-5\" style=\"\">
                            <h5><strong>";
        // line 585
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Contact information"), "html", null, true);
        echo "</strong></h5>
                            <p class=\"mt-4 mb-2\"><strong>";
        // line 586
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("+33633495607"), "html", null, true);
        echo "</strong></p>
                            <p class=\"mb-2\"><strong>PRO@ELIELALOUM.FR</strong></p>
                            <p class=\"mb-2\"><strong>PERSO@ELIELALOUM.FR</strong></p>
                            <p class=\"mb-5\"><strong>PARIS</strong></p>
                        </div>

                        <div class=\"col-lg-1\"></div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section id=\"mapSection\" class=\"content-section-c\">

        <style>
            #map {
                height: 400px;
                width: 100%;
            }
        </style>

        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-lg-12 pl-0 pr-0\">
                    <div id=\"map\">
                        test
                    </div>
                </div>
            </div>
        </div>

        <script>

            function initMap() {
                var Paris = {lat:48.857708, lng: 2.351865};

                var map = new google.maps.Map(document.getElementById('map'), {
                    center: Paris,
                    zoom: 8,
                    styles: [
                        {
                            \"featureType\": \"all\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"weight\": \"2.00\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"all\",
                            \"elementType\": \"geometry.stroke\",
                            \"stylers\": [
                                {
                                    \"color\": \"#9c9c9c\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"all\",
                            \"elementType\": \"labels.text\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"on\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"landscape\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"color\": \"#f2f2f2\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"landscape\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#ffffff\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"landscape.man_made\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#ffffff\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"poi\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"off\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"saturation\": -100
                                },
                                {
                                    \"lightness\": 45
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#eeeeee\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road\",
                            \"elementType\": \"labels.text.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#7b7b7b\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road\",
                            \"elementType\": \"labels.text.stroke\",
                            \"stylers\": [
                                {
                                    \"color\": \"#ffffff\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road.highway\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"simplified\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road.arterial\",
                            \"elementType\": \"labels.icon\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"off\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"transit\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"off\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"water\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"color\": \"#46bcec\"
                                },
                                {
                                    \"visibility\": \"on\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"water\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#c8d7d4\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"water\",
                            \"elementType\": \"labels.text.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#070707\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"water\",
                            \"elementType\": \"labels.text.stroke\",
                            \"stylers\": [
                                {
                                    \"color\": \"#ffffff\"
                                }
                            ]
                        }
                    ]
                });

                var marker = new google.maps.Marker({
                    position: Paris,
                    map: map,
                    title: 'Hello World!'
                });
            }
        </script>

        <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyClhvCnLGjzJ_tD4tjT5c7Sn1DlBPOpQKQ&callback=initMap\"></script>

    </section>

    <!-- Footer -->
    <footer>
        <div class=\"container\">
            <ul class=\"list-inline\">
                <li class=\"list-inline-item\">
                    <a href=\"#\">";
        // line 820
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Home"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#about\">";
        // line 824
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("About"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#services\">";
        // line 828
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Services"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#services\">";
        // line 832
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Works"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#contact\">";
        // line 836
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Contact"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#\">Back-Office</a>
                </li>
            </ul>
            <p class=\"copyright text-muted small\">Copyright &copy; Elie Laloum 2017. All Rights Reserved</p>
        </div>
    </footer>




";
        
        $__internal_8a9d3eb7fcc97ef38410fa8c02378448dd07a7847c733b794e74dd60a37d9f7c->leave($__internal_8a9d3eb7fcc97ef38410fa8c02378448dd07a7847c733b794e74dd60a37d9f7c_prof);

        
        $__internal_7f4c773950681406c7114ddaa16f43f6fde6fbe52c1590ed589a614eae6402d2->leave($__internal_7f4c773950681406c7114ddaa16f43f6fde6fbe52c1590ed589a614eae6402d2_prof);

    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle:Portfolio:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1118 => 836,  1111 => 832,  1104 => 828,  1097 => 824,  1090 => 820,  853 => 586,  849 => 585,  835 => 574,  830 => 572,  826 => 571,  822 => 570,  815 => 566,  805 => 559,  801 => 558,  759 => 519,  751 => 514,  747 => 513,  720 => 489,  712 => 484,  708 => 483,  681 => 459,  673 => 454,  644 => 428,  636 => 423,  611 => 401,  607 => 400,  603 => 399,  595 => 394,  570 => 372,  566 => 371,  558 => 366,  528 => 339,  506 => 320,  500 => 317,  449 => 269,  426 => 249,  422 => 248,  416 => 245,  408 => 240,  404 => 239,  398 => 236,  382 => 223,  378 => 221,  374 => 220,  366 => 215,  360 => 212,  344 => 199,  340 => 198,  335 => 196,  327 => 191,  321 => 188,  310 => 180,  306 => 179,  302 => 178,  293 => 172,  273 => 155,  269 => 154,  255 => 143,  240 => 131,  226 => 120,  206 => 103,  196 => 96,  186 => 89,  176 => 82,  157 => 66,  151 => 63,  147 => 62,  143 => 61,  119 => 40,  115 => 39,  111 => 38,  107 => 37,  87 => 20,  77 => 12,  68 => 11,  54 => 6,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EZPortfolioBundle::Layout/layout.html.twig\" %}

{% block title %}

    <title>
    Home - {{ parent() }}
    </title>

{% endblock %}

{% block ezhome_body %}

    <!-- Page Content -->
    <section id=\"about\" class=\"content-section-a\">

        <div class=\"container\">
            <div class=\"row\">

                <div class=\"col-lg-5 mr-auto overlayrepere\">
                    <img style=\"height: 90%\" class=\"img-about img-fluid mb-0 mt-auto\" src=\"{{ asset('bundles/webshome/img/default_avatar.jpg') }}\">
                    <div class=\"overlay\">
                        <div class=\"row\">
                            <div class=\"col-lg-12 text-center\">
                                <a href=\"#\" class=\"fa fa-facebook-square fa-3x\"></a>
                                <a href=\"#\" class=\"fa fa-twitter-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-linkedin-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-github-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-google-plus-square  fa-3x ml-1\"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"smallScreenSpace col-lg-5 ml-auto\">
                    <hr class=\"section-heading-spacer\">
                    <div class=\"clearfix\"></div>
                    <h2 class=\"section-heading\">{{ 'ABOUT ME'|trans }}</h2>
                    <p class=\"lead\">{{ 'Graduate from the A-Level STI2D option SIN and Senior technician patent Computer services to the organizations, I am currently doing a Licence Analyst & Computer Engineering.'|trans }}<br><br>
                        {{ 'Passionate about computer science and technology in general I am fascinated by the understanding of the different systems around us.'|trans }}<br><br>
                        {{ 'Ambitious and motivated, I am actively looking for an alternative training that will allow me to deploy my skills and talents within a professional infrastructure.'|trans }}</p>
                </div>
            </div>
        </div>

    </section>

    <section class=\"content-section-c\">

        <div id=\"skills\" class=\"container-fluid\">

            <div class=\"row justify-content-center\">

                <div class=\"col-lg-6 section-skills align-self-center\">

                    <div class=\"container\">

                        <div class=\"row justify-content-center\">

                            <div class=\"col-lg-9\">

                                <h2>{{ 'MY SKILLS'|trans }}</h2><br>
                                <h4>{{ 'SERIOUS SKILLS BUT KEEP LEARNING'|trans }}</h4>
                                <p>{{ 'In 3 years of learning I acquired a lot of knowledge that already allows me to meet the expectations of a project.'|trans }}</p>

                                <a href=\"#works\" class=\"networklink\">
                                    <span class=\"network-name\">{{ 'SEE MY WORKS'|trans }}</span>
                                </a>

                            </div>

                        </div>

                    </div>

                </div>

                <div class=\"col-lg-1\"></div>

                <div class=\"col-lg-4 mr-auto order-lg-2 justify-content-center align-self-center\">

                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">{{ 'WEB DEV'|trans }}</p>
                        <div class=\"skillbar clearfix \" data-percent=\"90%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">90%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">{{ 'WEBDESIGN'|trans }}</p>
                        <div class=\"skillbar clearfix \" data-percent=\"70%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">70%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">{{ 'SOFTWARE DEV'|trans }}</p>
                        <div class=\"skillbar clearfix \" data-percent=\"60%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">60%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">{{ 'APP DEV'|trans }}</p>
                        <div class=\"skillbar clearfix \" data-percent=\"50%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">50%</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id=\"services\" class=\"content-section-a text-center\">
        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>{{ 'SERVICES I OFFER'|trans }}</h2>

                    <div class=\"row service-row justify-content-center\">

                        <div class=\"col-lg-3 col-sm-6 col-md-6 col-xs-6 mt-4\">

                                <div id=\"st1\" class=\"service-thumb\">

                                    <i class=\"fa fa-wordpress fa-4x\"></i>

                                    <h3>CMS</h3>
                                    <p class=\"lead\">{{ 'For a project that can be modularized and stylised with fast handling.'|trans }}</p>
                                </div>

                        </div>

                        <div class=\"col-lg-3 col-sm-6 col-md-6 col-xs-6 mt-4\">

                                <div id=\"st2\" class=\"service-thumb\">

                                    <i class=\"faicon fa fa-paint-brush  fa-4x\"></i>

                                    <h3>WEBDESIGN</h3>
                                    <p class=\"lead\">{{ 'Template styliser according to specifications.'|trans }}</p>
                                </div>

                        </div>

                        <div class=\"col-lg-3 col-sm-12 col-md-12 col-xs-12 mt-4\">

                                <div id=\"st3\" class=\"service-thumb\">

                                    <i class=\"fa fa-code fa-4x\"></i>

                                    <h3>{{ 'DEVELOPEMENT'|trans }}</h3>
                                    <p class=\"lead\">{{ 'Heavy client development, mobile app or web service.'|trans }}</p>
                                </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id=\"works\" class=\"content-section-b text-center\">
        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>{{ 'MY WORKS'|trans }}</h2>
                    <br>

                    <div class=\"row justify-content-center\">

                        <div class=\"col-lg-12\">
                            <button class=\"tagBtn\" onclick=\"showTag('personal'); hideTag('school'); hideTag('professional')\">{{ 'Personal'|trans }}</button>
                            <button class=\"tagBtn\" onclick=\"showTag('school'); hideTag('personal'); hideTag('professional')\">{{ 'School'|trans }}</button>
                            <button class=\"tagBtn\" onclick=\"showTag('professional'); hideTag('personal'); hideTag('school')\">{{ 'Professional'|trans }}</button>
                        </div>

                        <div style=\"\" class=\"col-lg-8 mt-5 personal\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"{{ path('ez_portfolio_portfolio', {'src': 'scplateform'}) }}\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">SC Plateform</h5>
                                        <p class=\"col-lg-12\">{{ 'No describe for now.'|trans }}</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"{{ path('ez_portfolio_portfolio', {'src': 'customlocalhost'}) }}\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">{{ 'Custom localhost'|trans }}</h5>
                                        <p class=\"col-lg-12\">{{ 'HTML/CSS/JS System for custom localhost ( listing projects , custom links ).'|trans }}</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                        <div style=\"display: none;\" class=\"col-lg-8 mt-5 professional\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"{{ path('ez_portfolio_portfolio', {'src': 'guesswhat'}) }}\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">GuessWhat</h5>
                                        <p class=\"col-lg-12\">{{ 'Development of a mobile comparator in HTML/CSS/PHP.'|trans }}</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"{{ path('ez_portfolio_portfolio', {'src': 'igaboho'}) }}\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"{#{{ asset('bundles/webshome/img/projects/igaboho.svg') }}#}http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">Iga Boho Chic</h5>
                                        <p class=\"col-lg-12\">{{ 'Showcase site for an infrastructure of jewelry sales and seasonal clothing.'|trans }}</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                        <div style=\"display: none;\" class=\"col-lg-8 mt-5 school\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"{{ path('ez_portfolio_portfolio', {'src': 'ppe1'}) }}\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">PPE 1</h5>
                                        <p class=\"col-lg-12\">{{ 'Diploma Validation Project.'|trans }}</p>
                                        <p class=\"col-lg-12\">{{ 'Web client for customers of a car rental company.'|trans }}</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"{{ path('ez_portfolio_portfolio', {'src': 'ppe2'}) }}\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">PPE 2</h5>
                                        <p class=\"col-lg-12\">{{ 'Diploma Validation Project.'|trans }}</p>
                                        <p class=\"col -lg-12\">{{ 'Heavy client for the management of the car rental company database.'|trans }}</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id=\"experiences\" class=\"content-section-a text-center\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>{{ 'MY EXPERIENCE'|trans }}</h2>

                    <div class=\"row\">

                        <div class=\"frame col-lg-12\">

                            <!-- SMALL SCREEN -->

                            <div class=\"row justify-content-center ExpSmall\">
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-5\">
                                    <div class=\"row\">
                                        <div class=\"col-4 logopart\">
                                            <i class=\"mt-4 fa fa-code fa-3x\" aria-hidden=\"true\"></i>
                                        </div>
                                        <div class=\"descpart col-8 text-left\">
                                            <ul class=\"mt-1\">
                                                <li>GUESSWHAT</li>
                                                <li>2017</li>
                                            </ul>
                                            <p>Mauris balqnuet lorem ipsus dolores minum aerum tanges.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 2</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 3</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 4</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 5</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 6</p>
                                </div>
                            </div>

                            <!-- COMPUTER SCREEN -->

                            <div class=\"row mt-5\">

                                <div class=\"col-lg-12 text-center\">

                                    <div class=\"row\">
                                        <div class=\"col-lg-6 text-left\">
                                            <h3>{{ 'Professional'|trans }}</h3>
                                        </div>
                                        <div class=\"col-lg-6 text-right\">
                                            <h3>{{ 'Training'|trans }}</h3>
                                        </div>
                                    </div>

                                    <div class=\"row mt-4\">

                                        <div class=\"leftexp col-lg-6 mt-3\" style=\"border-left: 4px solid grey\">

                                            <div class=\"row\">
                                                <div class=\"year col-lg-2 mr-0 ml-auto pr-0 pl-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2017</p>
                                                    <hr style=\"border-bottom: 1px dotted grey\">
                                                </div>
                                                <div class=\"thumbexp col-lg-9 pr-0\" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-briefcase fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">{{ 'WORK'|trans }}</p>
                                                        </div>

                                                        <div class=\"col-lg-8 text-right\" style=\"border-left: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">BNP PARIBAS</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"col\"></div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"year col-lg-2 mr-0 ml-auto pr-0 pl-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2017</p>
                                                    <hr style=\"border-bottom: 1px dotted grey\">
                                                </div>
                                                <div class=\"thumbexp col-lg-9 pr-0\" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-briefcase fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">{{ 'TRAINEE'|trans }}</p>
                                                        </div>

                                                        <div class=\"col-lg-8 text-right\" style=\"border-left: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">GUESSWHAT</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">{{ 'Mobile suscription comparator.'|trans }}
                                                            <br>{{ 'Integration to another website.'|trans }}</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"col\"></div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"year col-lg-2 mr-0 ml-auto pr-0 pl-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2016</p>
                                                    <hr style=\"border-bottom: 1px dotted grey\">
                                                </div>
                                                <div class=\"thumbexp col-lg-9 pr-0\" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-briefcase fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">{{ 'PROJECT'|trans }}</p>
                                                        </div>

                                                        <div class=\"col-lg-8 text-right\" style=\"border-left: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">PPE</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">{{ 'Web service dev for rent car company.'|trans }}
                                                            <br>{{ 'Heavy client to manage db.'|trans }}
                                                            <br>{{ 'Mobile app for clients.'|trans }}</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"col\"></div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"year col-lg-2 mr-0 ml-auto pr-0 pl-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2016</p>
                                                    <hr style=\"border-bottom: 1px dotted grey\">
                                                </div>
                                                <div class=\"thumbexp col-lg-9 pr-0\" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-briefcase fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">{{ 'TRAINEE'|trans }}</p>
                                                        </div>

                                                        <div class=\"col-lg-8 text-right\" style=\"border-left: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">TECHNISITES</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">{{ 'Development of a web service for the verification of the state of printers of the computer park.'|trans }}
                                                            <br>HTML/CSS - PHP/SNMP</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"col\"></div>

                                            </div>

                                        </div>

                                        <div class=\"rightexp col-lg-6 mt-3\" style=\"border-right: 4px solid grey; max-height: 500px;\">

                                            <div class=\"row\">
                                                <div class=\"col\"></div>
                                                <div class=\"thumbexp col-lg-9 \" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col\"></div>

                                                        <div class=\"col-lg-8 text-left\" style=\"border-right: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">CFA INSTA</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">{{ 'Analyst & Computer Engineering, Development.'|trans }}</p>
                                                        </div>

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-graduation-cap fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">{{ 'BACHELOR'|trans }}</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"year col-lg-2 mr-0 ml-auto pl-0 pr-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2018</p>
                                                    <hr>
                                                </div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"col\"></div>
                                                <div class=\"thumbexp col-lg-9 \" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col\"></div>

                                                        <div class=\"col-lg-8 text-left\" style=\"border-right: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">CFA INSTA</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">{{ 'IT Service to Organizations.'|trans }}
                                                             <br>{{ 'Software solutions and business applications.'|trans }}</p>
                                                        </div>

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-graduation-cap fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">{{ '12th GR'|trans }}</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"year col-lg-2 mr-0 ml-auto pl-0 pr-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2017</p>
                                                    <hr>
                                                </div>

                                            </div>

                                            <div class=\"row mt-4\">
                                                <div class=\"col\"></div>
                                                <div class=\"thumbexp col-lg-9 \" style=\"border: 1px solid grey; height: 150px; border-radius: 10px;\">

                                                    <div class=\"row\">

                                                        <div class=\"col\"></div>

                                                        <div class=\"col-lg-8 text-left\" style=\"border-right: 1px solid grey; height:149px;\">
                                                            <h4 class=\"mt-4\">JEAN PERRIN</h4>
                                                            <p class=\"mt-2\" style=\"font-size: 14px;\">{{ 'Science and Technology for Industry and Sustainable Development.'|trans }}
                                                            <br>{{ 'Option Science Industrial & Digital.'|trans }}</p>
                                                        </div>

                                                        <div class=\"col-lg-3\">
                                                            <i class=\"fa fa-graduation-cap fa-2x mt-4\" aria-hidden=\"true\"></i>
                                                            <p class=\"mt-4\">{{ 'A-LVL'|trans }}</p>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class=\"year col-lg-2 mr-0 ml-auto pl-0 pr-auto\">
                                                    <p class=\"pt-auto pb-0\" style=\"font-size: 12px;\">2014</p>
                                                    <hr>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </div>
    </section>

    <section id=\"contact\" class=\"content-section-c text-center\">

        <div class=\"container-fluid\">

            <div class=\"row\">

                <div class=\"col-lg-12 section-contact\">
                    <h2 class=\"mt-5\">{{ 'CONTACT ME'|trans }}</h2>
                    <p>{{ 'Do not hesitate to contact me if you have any request'|trans }}</p>

                    <div class=\"row mb-5\">

                        <div class=\"col-lg-9 mt-5\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8 text-left\">
                                    <h5><strong>{{ 'Post a message'|trans }}</strong></h5>
                                    <form>
                                        <div class=\"form-group\">
                                            <div class=\"mt-4 justify-content-center\">
                                                <input type=\"email\" class=\"emailInput mt-2 col-lg-7 col-10 form-control\" id=\"exampleFormControlInput1\" placeholder=\"{{ 'Name@example.com'|trans }}\">
                                                <input type=\"object\" class=\"objectInput mt-2 col-lg-7 col-10 form-control\" id=\"exampleInputEmail1\" placeholder=\"{{ 'Enter subject'|trans }}\">
                                                <input type=\"tel\" class=\"telInput mt-2 col-lg-7 col-10 form-control\" id=\"exampleInputTel\" placeholder=\"{{ 'Cellphone number'|trans }}\">
                                                <textarea class=\"mt-2 messageInput form-control col-lg-11\" placeholder=\"Message\" id=\"exampleFormControlTextarea1\" rows=\"5\"></textarea>
                                                <button type=\"button\" class=\"mt-2 btn btn-lg col-lg-12 col-12\" style=\"background-color: white; color: gray;\">{{ 'Send Message'|trans }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <hr class=\"defdivider\">

                        <div class=\"infoContact col-lg-2 mt-5 text-left mb-5\" style=\"\">
                            <h5><strong>{{ 'Contact information'|trans }}</strong></h5>
                            <p class=\"mt-4 mb-2\"><strong>{{ '+33633495607'|trans }}</strong></p>
                            <p class=\"mb-2\"><strong>PRO@ELIELALOUM.FR</strong></p>
                            <p class=\"mb-2\"><strong>PERSO@ELIELALOUM.FR</strong></p>
                            <p class=\"mb-5\"><strong>PARIS</strong></p>
                        </div>

                        <div class=\"col-lg-1\"></div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section id=\"mapSection\" class=\"content-section-c\">

        <style>
            #map {
                height: 400px;
                width: 100%;
            }
        </style>

        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-lg-12 pl-0 pr-0\">
                    <div id=\"map\">
                        test
                    </div>
                </div>
            </div>
        </div>

        <script>

            function initMap() {
                var Paris = {lat:48.857708, lng: 2.351865};

                var map = new google.maps.Map(document.getElementById('map'), {
                    center: Paris,
                    zoom: 8,
                    styles: [
                        {
                            \"featureType\": \"all\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"weight\": \"2.00\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"all\",
                            \"elementType\": \"geometry.stroke\",
                            \"stylers\": [
                                {
                                    \"color\": \"#9c9c9c\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"all\",
                            \"elementType\": \"labels.text\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"on\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"landscape\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"color\": \"#f2f2f2\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"landscape\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#ffffff\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"landscape.man_made\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#ffffff\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"poi\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"off\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"saturation\": -100
                                },
                                {
                                    \"lightness\": 45
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#eeeeee\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road\",
                            \"elementType\": \"labels.text.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#7b7b7b\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road\",
                            \"elementType\": \"labels.text.stroke\",
                            \"stylers\": [
                                {
                                    \"color\": \"#ffffff\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road.highway\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"simplified\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"road.arterial\",
                            \"elementType\": \"labels.icon\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"off\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"transit\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"visibility\": \"off\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"water\",
                            \"elementType\": \"all\",
                            \"stylers\": [
                                {
                                    \"color\": \"#46bcec\"
                                },
                                {
                                    \"visibility\": \"on\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"water\",
                            \"elementType\": \"geometry.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#c8d7d4\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"water\",
                            \"elementType\": \"labels.text.fill\",
                            \"stylers\": [
                                {
                                    \"color\": \"#070707\"
                                }
                            ]
                        },
                        {
                            \"featureType\": \"water\",
                            \"elementType\": \"labels.text.stroke\",
                            \"stylers\": [
                                {
                                    \"color\": \"#ffffff\"
                                }
                            ]
                        }
                    ]
                });

                var marker = new google.maps.Marker({
                    position: Paris,
                    map: map,
                    title: 'Hello World!'
                });
            }
        </script>

        <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyClhvCnLGjzJ_tD4tjT5c7Sn1DlBPOpQKQ&callback=initMap\"></script>

    </section>

    <!-- Footer -->
    <footer>
        <div class=\"container\">
            <ul class=\"list-inline\">
                <li class=\"list-inline-item\">
                    <a href=\"#\">{{ 'Home'|trans }}</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#about\">{{ 'About'|trans }}</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#services\">{{ 'Services'|trans }}</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#services\">{{ 'Works'|trans }}</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#contact\">{{ 'Contact'|trans }}</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#\">Back-Office</a>
                </li>
            </ul>
            <p class=\"copyright text-muted small\">Copyright &copy; Elie Laloum 2017. All Rights Reserved</p>
        </div>
    </footer>




{% endblock %}", "EZPortfolioBundle:Portfolio:index.html.twig", "C:\\wamp64\\www\\personal\\EZone\\src\\EZ\\PortfolioBundle/Resources/views/Portfolio/index.html.twig");
    }
}
