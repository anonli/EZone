<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_45210378ad38c86f804adad8123ee2e01207f245b419baad86a07fba02c444de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff31dd3da29cf86ea3c03656ad3c51ac3aa9ea03fca337315d6cfacca90120cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff31dd3da29cf86ea3c03656ad3c51ac3aa9ea03fca337315d6cfacca90120cb->enter($__internal_ff31dd3da29cf86ea3c03656ad3c51ac3aa9ea03fca337315d6cfacca90120cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_88309e9b0d64ab9af4b9f0691133e9109192e0a9e07a363109d12323354975fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88309e9b0d64ab9af4b9f0691133e9109192e0a9e07a363109d12323354975fe->enter($__internal_88309e9b0d64ab9af4b9f0691133e9109192e0a9e07a363109d12323354975fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_ff31dd3da29cf86ea3c03656ad3c51ac3aa9ea03fca337315d6cfacca90120cb->leave($__internal_ff31dd3da29cf86ea3c03656ad3c51ac3aa9ea03fca337315d6cfacca90120cb_prof);

        
        $__internal_88309e9b0d64ab9af4b9f0691133e9109192e0a9e07a363109d12323354975fe->leave($__internal_88309e9b0d64ab9af4b9f0691133e9109192e0a9e07a363109d12323354975fe_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5335759551b0a7aeea372d036bd527d525fe97c54d3bf3f817a1ead9177fe4d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5335759551b0a7aeea372d036bd527d525fe97c54d3bf3f817a1ead9177fe4d9->enter($__internal_5335759551b0a7aeea372d036bd527d525fe97c54d3bf3f817a1ead9177fe4d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_0659526afcfdac05e9a993d3b8c66b8e1696017cbc97825024ac0218f4c3aab9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0659526afcfdac05e9a993d3b8c66b8e1696017cbc97825024ac0218f4c3aab9->enter($__internal_0659526afcfdac05e9a993d3b8c66b8e1696017cbc97825024ac0218f4c3aab9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_0659526afcfdac05e9a993d3b8c66b8e1696017cbc97825024ac0218f4c3aab9->leave($__internal_0659526afcfdac05e9a993d3b8c66b8e1696017cbc97825024ac0218f4c3aab9_prof);

        
        $__internal_5335759551b0a7aeea372d036bd527d525fe97c54d3bf3f817a1ead9177fe4d9->leave($__internal_5335759551b0a7aeea372d036bd527d525fe97c54d3bf3f817a1ead9177fe4d9_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_8356a235ad28c9130bbbb4bb0539d33c0d9a64b0aed6d2d00d7397663dc7bd43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8356a235ad28c9130bbbb4bb0539d33c0d9a64b0aed6d2d00d7397663dc7bd43->enter($__internal_8356a235ad28c9130bbbb4bb0539d33c0d9a64b0aed6d2d00d7397663dc7bd43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_408c5ed5ef189e00acb71d04d1915b46272725b30a35f02009b163edffdfcfa6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_408c5ed5ef189e00acb71d04d1915b46272725b30a35f02009b163edffdfcfa6->enter($__internal_408c5ed5ef189e00acb71d04d1915b46272725b30a35f02009b163edffdfcfa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_408c5ed5ef189e00acb71d04d1915b46272725b30a35f02009b163edffdfcfa6->leave($__internal_408c5ed5ef189e00acb71d04d1915b46272725b30a35f02009b163edffdfcfa6_prof);

        
        $__internal_8356a235ad28c9130bbbb4bb0539d33c0d9a64b0aed6d2d00d7397663dc7bd43->leave($__internal_8356a235ad28c9130bbbb4bb0539d33c0d9a64b0aed6d2d00d7397663dc7bd43_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_0055cd90424a83776f3e7569917f567a7a7db16c91864175994918cdb044e109 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0055cd90424a83776f3e7569917f567a7a7db16c91864175994918cdb044e109->enter($__internal_0055cd90424a83776f3e7569917f567a7a7db16c91864175994918cdb044e109_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a2d5ba153d4bcb660d74e02d323877ea49771cfb5c157fe60ba317bcdcdaf6a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2d5ba153d4bcb660d74e02d323877ea49771cfb5c157fe60ba317bcdcdaf6a8->enter($__internal_a2d5ba153d4bcb660d74e02d323877ea49771cfb5c157fe60ba317bcdcdaf6a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_a2d5ba153d4bcb660d74e02d323877ea49771cfb5c157fe60ba317bcdcdaf6a8->leave($__internal_a2d5ba153d4bcb660d74e02d323877ea49771cfb5c157fe60ba317bcdcdaf6a8_prof);

        
        $__internal_0055cd90424a83776f3e7569917f567a7a7db16c91864175994918cdb044e109->leave($__internal_0055cd90424a83776f3e7569917f567a7a7db16c91864175994918cdb044e109_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "C:\\wamp64\\www\\personal\\EZone\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
