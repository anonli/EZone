<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_298b9d532c6f95ae216b7e059274db33509b27f2aacdee327036a4a77528d802 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed49234803e10dea24689e83094501c7bf09298cdcb155a2e5cebaef92a6a753 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed49234803e10dea24689e83094501c7bf09298cdcb155a2e5cebaef92a6a753->enter($__internal_ed49234803e10dea24689e83094501c7bf09298cdcb155a2e5cebaef92a6a753_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_bfb7901ce80745c773d0f27f2580bae86898576209445cd3c090d18a98f69c42 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfb7901ce80745c773d0f27f2580bae86898576209445cd3c090d18a98f69c42->enter($__internal_bfb7901ce80745c773d0f27f2580bae86898576209445cd3c090d18a98f69c42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed49234803e10dea24689e83094501c7bf09298cdcb155a2e5cebaef92a6a753->leave($__internal_ed49234803e10dea24689e83094501c7bf09298cdcb155a2e5cebaef92a6a753_prof);

        
        $__internal_bfb7901ce80745c773d0f27f2580bae86898576209445cd3c090d18a98f69c42->leave($__internal_bfb7901ce80745c773d0f27f2580bae86898576209445cd3c090d18a98f69c42_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_25b57776898d0a2b1d455b98a7780ba9aefcd4447f59ee1577f8ff40893614db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25b57776898d0a2b1d455b98a7780ba9aefcd4447f59ee1577f8ff40893614db->enter($__internal_25b57776898d0a2b1d455b98a7780ba9aefcd4447f59ee1577f8ff40893614db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_3a8133044477cd0afff51bc1a56b0b65154f1ae5301f35fcf2bdca4a1bb53062 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a8133044477cd0afff51bc1a56b0b65154f1ae5301f35fcf2bdca4a1bb53062->enter($__internal_3a8133044477cd0afff51bc1a56b0b65154f1ae5301f35fcf2bdca4a1bb53062_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_3a8133044477cd0afff51bc1a56b0b65154f1ae5301f35fcf2bdca4a1bb53062->leave($__internal_3a8133044477cd0afff51bc1a56b0b65154f1ae5301f35fcf2bdca4a1bb53062_prof);

        
        $__internal_25b57776898d0a2b1d455b98a7780ba9aefcd4447f59ee1577f8ff40893614db->leave($__internal_25b57776898d0a2b1d455b98a7780ba9aefcd4447f59ee1577f8ff40893614db_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f76477f5a478968159eb3b607fd8f39efd9656fbefa7ae992ee4fdeeed62d2e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f76477f5a478968159eb3b607fd8f39efd9656fbefa7ae992ee4fdeeed62d2e3->enter($__internal_f76477f5a478968159eb3b607fd8f39efd9656fbefa7ae992ee4fdeeed62d2e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_69bbe0ffc3ead7e46b093cb63a2a4bdc75747b9e2c8660cbb2327b543e0d76dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69bbe0ffc3ead7e46b093cb63a2a4bdc75747b9e2c8660cbb2327b543e0d76dd->enter($__internal_69bbe0ffc3ead7e46b093cb63a2a4bdc75747b9e2c8660cbb2327b543e0d76dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_69bbe0ffc3ead7e46b093cb63a2a4bdc75747b9e2c8660cbb2327b543e0d76dd->leave($__internal_69bbe0ffc3ead7e46b093cb63a2a4bdc75747b9e2c8660cbb2327b543e0d76dd_prof);

        
        $__internal_f76477f5a478968159eb3b607fd8f39efd9656fbefa7ae992ee4fdeeed62d2e3->leave($__internal_f76477f5a478968159eb3b607fd8f39efd9656fbefa7ae992ee4fdeeed62d2e3_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a0f872fe89eddb295f1f29f4b05494a7e61030142c0d9a7f6b2a7955f32629ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0f872fe89eddb295f1f29f4b05494a7e61030142c0d9a7f6b2a7955f32629ca->enter($__internal_a0f872fe89eddb295f1f29f4b05494a7e61030142c0d9a7f6b2a7955f32629ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_957849fc6140780b9e50c8356b7218bb212cf8f2321a0a01a8e2c7a362a7b197 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_957849fc6140780b9e50c8356b7218bb212cf8f2321a0a01a8e2c7a362a7b197->enter($__internal_957849fc6140780b9e50c8356b7218bb212cf8f2321a0a01a8e2c7a362a7b197_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_957849fc6140780b9e50c8356b7218bb212cf8f2321a0a01a8e2c7a362a7b197->leave($__internal_957849fc6140780b9e50c8356b7218bb212cf8f2321a0a01a8e2c7a362a7b197_prof);

        
        $__internal_a0f872fe89eddb295f1f29f4b05494a7e61030142c0d9a7f6b2a7955f32629ca->leave($__internal_a0f872fe89eddb295f1f29f4b05494a7e61030142c0d9a7f6b2a7955f32629ca_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp64\\www\\personal\\EZone\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
