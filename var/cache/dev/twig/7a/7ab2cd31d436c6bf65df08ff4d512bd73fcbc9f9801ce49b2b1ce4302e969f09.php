<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_5d54ece49f3eaea3b8641a621aa5ea65eab7f2456c91cd9fb7e51d715f7116e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db81c1f3cb6aecd87a31085efd4bdaec37a56611ec8dec55e4231a2e97de494c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db81c1f3cb6aecd87a31085efd4bdaec37a56611ec8dec55e4231a2e97de494c->enter($__internal_db81c1f3cb6aecd87a31085efd4bdaec37a56611ec8dec55e4231a2e97de494c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_f839bfa8cbcbf6463e56054ca8951b330c95ef7e795e289077637d7f15245496 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f839bfa8cbcbf6463e56054ca8951b330c95ef7e795e289077637d7f15245496->enter($__internal_f839bfa8cbcbf6463e56054ca8951b330c95ef7e795e289077637d7f15245496_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_db81c1f3cb6aecd87a31085efd4bdaec37a56611ec8dec55e4231a2e97de494c->leave($__internal_db81c1f3cb6aecd87a31085efd4bdaec37a56611ec8dec55e4231a2e97de494c_prof);

        
        $__internal_f839bfa8cbcbf6463e56054ca8951b330c95ef7e795e289077637d7f15245496->leave($__internal_f839bfa8cbcbf6463e56054ca8951b330c95ef7e795e289077637d7f15245496_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e0056e0ed93b60c306e8a147e758f7111d1040bf535c1907fd5872ca2c15d2f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0056e0ed93b60c306e8a147e758f7111d1040bf535c1907fd5872ca2c15d2f2->enter($__internal_e0056e0ed93b60c306e8a147e758f7111d1040bf535c1907fd5872ca2c15d2f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e7322ee6c7e52bcad3d97fd3d6ab41ddfe6d565180198594b7a86fee711c54b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7322ee6c7e52bcad3d97fd3d6ab41ddfe6d565180198594b7a86fee711c54b2->enter($__internal_e7322ee6c7e52bcad3d97fd3d6ab41ddfe6d565180198594b7a86fee711c54b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_e7322ee6c7e52bcad3d97fd3d6ab41ddfe6d565180198594b7a86fee711c54b2->leave($__internal_e7322ee6c7e52bcad3d97fd3d6ab41ddfe6d565180198594b7a86fee711c54b2_prof);

        
        $__internal_e0056e0ed93b60c306e8a147e758f7111d1040bf535c1907fd5872ca2c15d2f2->leave($__internal_e0056e0ed93b60c306e8a147e758f7111d1040bf535c1907fd5872ca2c15d2f2_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_857c5354f16e69e339515131b4d3192ae0c7da9a0b9f990d3a1d1baa7dc37177 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_857c5354f16e69e339515131b4d3192ae0c7da9a0b9f990d3a1d1baa7dc37177->enter($__internal_857c5354f16e69e339515131b4d3192ae0c7da9a0b9f990d3a1d1baa7dc37177_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_7511f6b280aff99c7167981dad3a2908e20469143277e424c94f1f6b22489fdc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7511f6b280aff99c7167981dad3a2908e20469143277e424c94f1f6b22489fdc->enter($__internal_7511f6b280aff99c7167981dad3a2908e20469143277e424c94f1f6b22489fdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_7511f6b280aff99c7167981dad3a2908e20469143277e424c94f1f6b22489fdc->leave($__internal_7511f6b280aff99c7167981dad3a2908e20469143277e424c94f1f6b22489fdc_prof);

        
        $__internal_857c5354f16e69e339515131b4d3192ae0c7da9a0b9f990d3a1d1baa7dc37177->leave($__internal_857c5354f16e69e339515131b4d3192ae0c7da9a0b9f990d3a1d1baa7dc37177_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a31185f28602a4e10ea1cf3a14921537eb1b8808e84fa1911543775c650de8f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a31185f28602a4e10ea1cf3a14921537eb1b8808e84fa1911543775c650de8f4->enter($__internal_a31185f28602a4e10ea1cf3a14921537eb1b8808e84fa1911543775c650de8f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_793de492cb971a1c471cc2876ac20efcb77009db59ebe4df7d4af272eb50a99f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_793de492cb971a1c471cc2876ac20efcb77009db59ebe4df7d4af272eb50a99f->enter($__internal_793de492cb971a1c471cc2876ac20efcb77009db59ebe4df7d4af272eb50a99f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_793de492cb971a1c471cc2876ac20efcb77009db59ebe4df7d4af272eb50a99f->leave($__internal_793de492cb971a1c471cc2876ac20efcb77009db59ebe4df7d4af272eb50a99f_prof);

        
        $__internal_a31185f28602a4e10ea1cf3a14921537eb1b8808e84fa1911543775c650de8f4->leave($__internal_a31185f28602a4e10ea1cf3a14921537eb1b8808e84fa1911543775c650de8f4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp64\\www\\personal\\EZone\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
