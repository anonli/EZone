<?php

/* CoreSphereConsoleBundle:Console:console.html.twig */
class __TwigTemplate_05f71ddf22d85f21cb02f04386fdf638909d46b693e9e51aee709fe5de800f1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CoreSphereConsoleBundle::base.html.twig", "CoreSphereConsoleBundle:Console:console.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreSphereConsoleBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_989d21ac9175f2cd37e2bbd504026d48c5bf3b87ad24d4751079332d8a9704a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_989d21ac9175f2cd37e2bbd504026d48c5bf3b87ad24d4751079332d8a9704a3->enter($__internal_989d21ac9175f2cd37e2bbd504026d48c5bf3b87ad24d4751079332d8a9704a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreSphereConsoleBundle:Console:console.html.twig"));

        $__internal_dd64d15555caca4381aad3de0e2aaa746e94fcf1979fda52ab61ca6c5b15cb01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd64d15555caca4381aad3de0e2aaa746e94fcf1979fda52ab61ca6c5b15cb01->enter($__internal_dd64d15555caca4381aad3de0e2aaa746e94fcf1979fda52ab61ca6c5b15cb01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreSphereConsoleBundle:Console:console.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_989d21ac9175f2cd37e2bbd504026d48c5bf3b87ad24d4751079332d8a9704a3->leave($__internal_989d21ac9175f2cd37e2bbd504026d48c5bf3b87ad24d4751079332d8a9704a3_prof);

        
        $__internal_dd64d15555caca4381aad3de0e2aaa746e94fcf1979fda52ab61ca6c5b15cb01->leave($__internal_dd64d15555caca4381aad3de0e2aaa746e94fcf1979fda52ab61ca6c5b15cb01_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b73729ef6e2040c4b76208f9c0fb79b1a3025599d7012ef8cf19e6ed47447e26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b73729ef6e2040c4b76208f9c0fb79b1a3025599d7012ef8cf19e6ed47447e26->enter($__internal_b73729ef6e2040c4b76208f9c0fb79b1a3025599d7012ef8cf19e6ed47447e26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_beaf1e9b0e4b3d317a5d149cc5100b5deac686940278b8b14fa86bb79eb7eb77 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_beaf1e9b0e4b3d317a5d149cc5100b5deac686940278b8b14fa86bb79eb7eb77->enter($__internal_beaf1e9b0e4b3d317a5d149cc5100b5deac686940278b8b14fa86bb79eb7eb77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("coresphere_console.headline.index"), "html", null, true);
        
        $__internal_beaf1e9b0e4b3d317a5d149cc5100b5deac686940278b8b14fa86bb79eb7eb77->leave($__internal_beaf1e9b0e4b3d317a5d149cc5100b5deac686940278b8b14fa86bb79eb7eb77_prof);

        
        $__internal_b73729ef6e2040c4b76208f9c0fb79b1a3025599d7012ef8cf19e6ed47447e26->leave($__internal_b73729ef6e2040c4b76208f9c0fb79b1a3025599d7012ef8cf19e6ed47447e26_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_fe86376f0bd7349b790d31dd6a841441a297acbbece719cc373769580601db5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe86376f0bd7349b790d31dd6a841441a297acbbece719cc373769580601db5d->enter($__internal_fe86376f0bd7349b790d31dd6a841441a297acbbece719cc373769580601db5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_e4c841d6e02d69379cf436867060bbe78a2ce0586a7551d88e486fdb150374c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4c841d6e02d69379cf436867060bbe78a2ce0586a7551d88e486fdb150374c3->enter($__internal_e4c841d6e02d69379cf436867060bbe78a2ce0586a7551d88e486fdb150374c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/coresphereconsole/css/console.css"), "html", null, true);
        echo "\" type=\"text/css\" />
";
        
        $__internal_e4c841d6e02d69379cf436867060bbe78a2ce0586a7551d88e486fdb150374c3->leave($__internal_e4c841d6e02d69379cf436867060bbe78a2ce0586a7551d88e486fdb150374c3_prof);

        
        $__internal_fe86376f0bd7349b790d31dd6a841441a297acbbece719cc373769580601db5d->leave($__internal_fe86376f0bd7349b790d31dd6a841441a297acbbece719cc373769580601db5d_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_5f6beeb19c6b1b984a8ac269ed5ed9559f9e0e83816eeacf8ef0587d72bbdc7b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f6beeb19c6b1b984a8ac269ed5ed9559f9e0e83816eeacf8ef0587d72bbdc7b->enter($__internal_5f6beeb19c6b1b984a8ac269ed5ed9559f9e0e83816eeacf8ef0587d72bbdc7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_02ed987e336b1b026cad7fe00b0ffd845fdca4cf4e6ff4b6be52b4bd3ae6dabd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02ed987e336b1b026cad7fe00b0ffd845fdca4cf4e6ff4b6be52b4bd3ae6dabd->enter($__internal_02ed987e336b1b026cad7fe00b0ffd845fdca4cf4e6ff4b6be52b4bd3ae6dabd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "    ";
        $this->loadTemplate("CoreSphereConsoleBundle:Console:terminal.html.twig", "CoreSphereConsoleBundle:Console:console.html.twig", 11)->display($context);
        // line 12
        echo "    ";
        $this->loadTemplate("CoreSphereConsoleBundle:Console:htmlTemplates.html.twig", "CoreSphereConsoleBundle:Console:console.html.twig", 12)->display($context);
        
        $__internal_02ed987e336b1b026cad7fe00b0ffd845fdca4cf4e6ff4b6be52b4bd3ae6dabd->leave($__internal_02ed987e336b1b026cad7fe00b0ffd845fdca4cf4e6ff4b6be52b4bd3ae6dabd_prof);

        
        $__internal_5f6beeb19c6b1b984a8ac269ed5ed9559f9e0e83816eeacf8ef0587d72bbdc7b->leave($__internal_5f6beeb19c6b1b984a8ac269ed5ed9559f9e0e83816eeacf8ef0587d72bbdc7b_prof);

    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ccbe2acc07022b9bf6819def1b7e8b781de2a44aaca2ff04d21f138fac7e55e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ccbe2acc07022b9bf6819def1b7e8b781de2a44aaca2ff04d21f138fac7e55e1->enter($__internal_ccbe2acc07022b9bf6819def1b7e8b781de2a44aaca2ff04d21f138fac7e55e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_b53dc904929fdc45594e414e67e25bd5275fa05b2ac68a22e313a0d304fdd635 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b53dc904929fdc45594e414e67e25bd5275fa05b2ac68a22e313a0d304fdd635->enter($__internal_b53dc904929fdc45594e414e67e25bd5275fa05b2ac68a22e313a0d304fdd635_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 16
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/coresphereconsole/js/console.js"), "html", null, true);
        echo "\"></script>
    <script>
        jQuery(function () {
            ";
        // line 21
        echo "            var coresphere_console = new CoreSphereConsole(
                jQuery(\"#coresphere_consolebundle_console\"), {
                \"commands\" : ";
        // line 23
        echo twig_jsonencode_filter(twig_get_array_keys_filter((isset($context["commands"]) ? $context["commands"] : $this->getContext($context, "commands"))));
        echo ".sort(),
                \"post_path\" : \"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("console_exec"), "js", null, true);
        echo "\",
                \"environment\" : \"";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["environment"]) ? $context["environment"] : $this->getContext($context, "environment")), "js", null, true);
        echo "\",
                \"lang\" : {
                    \"loading\" : \"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("coresphere_console.loading"), "js", null, true);
        echo "\",
                    \"suggestion_head\" : \"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("coresphere_console.suggestion_head"), "js", null, true);
        echo "\",
                    \"environment\" : \"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("coresphere_console.environment"), "js", null, true);
        echo "\",
                    \"empty_response\" : \"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("coresphere_console.empty_response"), "js", null, true);
        echo "\",
                    \"welcome_message\" : \"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("coresphere_console.welcome_message", array("%command%" => "<code class=\"console_command\">list</code>")), "js", null, true);
        echo "\"
                },
                \"templates\" : {
                    \"error\" : \$(\"#template_console_error\").text(),
                    \"command\" : \$(\"#template_console_command\").text(),
                    \"environment\" : \$(\"#template_console_environment\").text(),
                    \"suggestion_list\" : \$('#template_suggestion_list').text(),
                    \"loading\" : \$('#template_console_loading').text(),
                    \"suggestion_item_active\" : \$('#suggestion_item_active').text(),
                    \"suggestion_item\" : \$('#suggestion_item').text(),
                    \"highlight\" : \$('#template_console_highlight').text().trim()
                }
            });
            ";
        // line 45
        echo "        });
    </script>
";
        
        $__internal_b53dc904929fdc45594e414e67e25bd5275fa05b2ac68a22e313a0d304fdd635->leave($__internal_b53dc904929fdc45594e414e67e25bd5275fa05b2ac68a22e313a0d304fdd635_prof);

        
        $__internal_ccbe2acc07022b9bf6819def1b7e8b781de2a44aaca2ff04d21f138fac7e55e1->leave($__internal_ccbe2acc07022b9bf6819def1b7e8b781de2a44aaca2ff04d21f138fac7e55e1_prof);

    }

    public function getTemplateName()
    {
        return "CoreSphereConsoleBundle:Console:console.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 45,  163 => 31,  159 => 30,  155 => 29,  151 => 28,  147 => 27,  142 => 25,  138 => 24,  134 => 23,  130 => 21,  124 => 17,  119 => 16,  110 => 15,  99 => 12,  96 => 11,  87 => 10,  75 => 7,  70 => 6,  61 => 5,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'CoreSphereConsoleBundle::base.html.twig'  %}

{% block title %}{{ 'coresphere_console.headline.index'|trans }}{% endblock %}

{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/coresphereconsole/css/console.css') }}\" type=\"text/css\" />
{% endblock %}

{% block body %}
    {%  include 'CoreSphereConsoleBundle:Console:terminal.html.twig' %}
    {%  include 'CoreSphereConsoleBundle:Console:htmlTemplates.html.twig' %}
{% endblock %}

{% block javascripts %}
    {{ parent() }}
    <script src=\"{{ asset('bundles/coresphereconsole/js/console.js') }}\"></script>
    <script>
        jQuery(function () {
            {% autoescape 'js' %}
            var coresphere_console = new CoreSphereConsole(
                jQuery(\"#coresphere_consolebundle_console\"), {
                \"commands\" : {{ commands|keys|json_encode|raw }}.sort(),
                \"post_path\" : \"{{ path('console_exec') }}\",
                \"environment\" : \"{{ environment }}\",
                \"lang\" : {
                    \"loading\" : \"{{ 'coresphere_console.loading'|trans }}\",
                    \"suggestion_head\" : \"{{ 'coresphere_console.suggestion_head'|trans }}\",
                    \"environment\" : \"{{ 'coresphere_console.environment'|trans }}\",
                    \"empty_response\" : \"{{ 'coresphere_console.empty_response'|trans }}\",
                    \"welcome_message\" : \"{{ 'coresphere_console.welcome_message'|trans({'%command%': '<code class=\\\"console_command\\\">list</code>'}) }}\"
                },
                \"templates\" : {
                    \"error\" : \$(\"#template_console_error\").text(),
                    \"command\" : \$(\"#template_console_command\").text(),
                    \"environment\" : \$(\"#template_console_environment\").text(),
                    \"suggestion_list\" : \$('#template_suggestion_list').text(),
                    \"loading\" : \$('#template_console_loading').text(),
                    \"suggestion_item_active\" : \$('#suggestion_item_active').text(),
                    \"suggestion_item\" : \$('#suggestion_item').text(),
                    \"highlight\" : \$('#template_console_highlight').text().trim()
                }
            });
            {% endautoescape %}
        });
    </script>
{% endblock %}
", "CoreSphereConsoleBundle:Console:console.html.twig", "C:\\wamp64\\www\\personal\\EZone\\vendor\\coresphere\\console-bundle/Resources/views/Console/console.html.twig");
    }
}
