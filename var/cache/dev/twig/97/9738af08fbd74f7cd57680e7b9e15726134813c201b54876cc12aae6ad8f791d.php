<?php

/* EZPortfolioBundle:Portfolio:portfolio_view.html.twig */
class __TwigTemplate_4f8e1963e62c308ed1ea3c8a33e19a6dfdb07323b6a8661501b275b63a4ee678 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EZPortfolioBundle::Layout/portfolio_layout.html.twig", "EZPortfolioBundle:Portfolio:portfolio_view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'ezportfolio_body' => array($this, 'block_ezportfolio_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EZPortfolioBundle::Layout/portfolio_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_095ef9309bca41904b6fb5757b8a14d7542521822d30da987bfabf94c98eb01b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_095ef9309bca41904b6fb5757b8a14d7542521822d30da987bfabf94c98eb01b->enter($__internal_095ef9309bca41904b6fb5757b8a14d7542521822d30da987bfabf94c98eb01b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle:Portfolio:portfolio_view.html.twig"));

        $__internal_cc7611095a2d42ed46863982d66e67537fc8db6cdbf224479e2798019b93fad7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc7611095a2d42ed46863982d66e67537fc8db6cdbf224479e2798019b93fad7->enter($__internal_cc7611095a2d42ed46863982d66e67537fc8db6cdbf224479e2798019b93fad7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle:Portfolio:portfolio_view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_095ef9309bca41904b6fb5757b8a14d7542521822d30da987bfabf94c98eb01b->leave($__internal_095ef9309bca41904b6fb5757b8a14d7542521822d30da987bfabf94c98eb01b_prof);

        
        $__internal_cc7611095a2d42ed46863982d66e67537fc8db6cdbf224479e2798019b93fad7->leave($__internal_cc7611095a2d42ed46863982d66e67537fc8db6cdbf224479e2798019b93fad7_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ce36080e8323d12f79dd7a1c01970c7c567ff98ea5f042d8ff4c6e1a8cc78ce5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce36080e8323d12f79dd7a1c01970c7c567ff98ea5f042d8ff4c6e1a8cc78ce5->enter($__internal_ce36080e8323d12f79dd7a1c01970c7c567ff98ea5f042d8ff4c6e1a8cc78ce5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_a1208bca9846b9f3ce443231adc312b1044d5ca19c077c172e39a59c7185ebfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1208bca9846b9f3ce443231adc312b1044d5ca19c077c172e39a59c7185ebfc->enter($__internal_a1208bca9846b9f3ce443231adc312b1044d5ca19c077c172e39a59c7185ebfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "
    <title>
        ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo " - ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
    </title>

";
        
        $__internal_a1208bca9846b9f3ce443231adc312b1044d5ca19c077c172e39a59c7185ebfc->leave($__internal_a1208bca9846b9f3ce443231adc312b1044d5ca19c077c172e39a59c7185ebfc_prof);

        
        $__internal_ce36080e8323d12f79dd7a1c01970c7c567ff98ea5f042d8ff4c6e1a8cc78ce5->leave($__internal_ce36080e8323d12f79dd7a1c01970c7c567ff98ea5f042d8ff4c6e1a8cc78ce5_prof);

    }

    // line 13
    public function block_ezportfolio_body($context, array $blocks = array())
    {
        $__internal_08dcbfb523570ec0b337ca0e9a8440d13fb44655b8b98c3eb393a930528c5aa1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08dcbfb523570ec0b337ca0e9a8440d13fb44655b8b98c3eb393a930528c5aa1->enter($__internal_08dcbfb523570ec0b337ca0e9a8440d13fb44655b8b98c3eb393a930528c5aa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezportfolio_body"));

        $__internal_0cb650a2a596b7436351be4c9bdd5ac33a6ec5814c0a5002bdec5cf5a68f1d5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cb650a2a596b7436351be4c9bdd5ac33a6ec5814c0a5002bdec5cf5a68f1d5a->enter($__internal_0cb650a2a596b7436351be4c9bdd5ac33a6ec5814c0a5002bdec5cf5a68f1d5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezportfolio_body"));

        // line 14
        echo "
    <iframe src=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["src"]) ? $context["src"] : $this->getContext($context, "src")), "html", null, true);
        echo "\" style=\"width: 100%; height: 100vh;\"></iframe>

";
        
        $__internal_0cb650a2a596b7436351be4c9bdd5ac33a6ec5814c0a5002bdec5cf5a68f1d5a->leave($__internal_0cb650a2a596b7436351be4c9bdd5ac33a6ec5814c0a5002bdec5cf5a68f1d5a_prof);

        
        $__internal_08dcbfb523570ec0b337ca0e9a8440d13fb44655b8b98c3eb393a930528c5aa1->leave($__internal_08dcbfb523570ec0b337ca0e9a8440d13fb44655b8b98c3eb393a930528c5aa1_prof);

    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle:Portfolio:portfolio_view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 15,  79 => 14,  70 => 13,  54 => 6,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EZPortfolioBundle::Layout/portfolio_layout.html.twig\" %}

{% block title %}

    <title>
        {{ name }} - {{ parent() }}
    </title>

{% endblock %}



{% block ezportfolio_body %}

    <iframe src=\"{{ src }}\" style=\"width: 100%; height: 100vh;\"></iframe>

{% endblock %}
", "EZPortfolioBundle:Portfolio:portfolio_view.html.twig", "C:\\wamp64\\www\\personal\\EZone\\src\\EZ\\PortfolioBundle/Resources/views/Portfolio/portfolio_view.html.twig");
    }
}
