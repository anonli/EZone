<?php

/* EZPortfolioBundle::Layout/portfolio_layout.html.twig */
class __TwigTemplate_f59c38352aae8e722e865aab64024f1b2094ce2509a492114cfc38bdfd50d753 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'ezportfolio_body' => array($this, 'block_ezportfolio_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6af890b5cc69059a9992bd5726e8a1f84b3750749348a1f900e9abe097dae1e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6af890b5cc69059a9992bd5726e8a1f84b3750749348a1f900e9abe097dae1e4->enter($__internal_6af890b5cc69059a9992bd5726e8a1f84b3750749348a1f900e9abe097dae1e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle::Layout/portfolio_layout.html.twig"));

        $__internal_b609d1a3908809be609ab61092f0dcd560e3d22909b7588a5e8c8b9e165d993f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b609d1a3908809be609ab61092f0dcd560e3d22909b7588a5e8c8b9e165d993f->enter($__internal_b609d1a3908809be609ab61092f0dcd560e3d22909b7588a5e8c8b9e165d993f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle::Layout/portfolio_layout.html.twig"));

        // line 1
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_6af890b5cc69059a9992bd5726e8a1f84b3750749348a1f900e9abe097dae1e4->leave($__internal_6af890b5cc69059a9992bd5726e8a1f84b3750749348a1f900e9abe097dae1e4_prof);

        
        $__internal_b609d1a3908809be609ab61092f0dcd560e3d22909b7588a5e8c8b9e165d993f->leave($__internal_b609d1a3908809be609ab61092f0dcd560e3d22909b7588a5e8c8b9e165d993f_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_60cc6bc684e94a7201d8a0232f09f501a4e1544c2cf0766c98eeaa8ca72e3372 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60cc6bc684e94a7201d8a0232f09f501a4e1544c2cf0766c98eeaa8ca72e3372->enter($__internal_60cc6bc684e94a7201d8a0232f09f501a4e1544c2cf0766c98eeaa8ca72e3372_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f58bbf1dc496907ca77e5a1df030a92b4d07495bd5050b23c66d88789a945297 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f58bbf1dc496907ca77e5a1df030a92b4d07495bd5050b23c66d88789a945297->enter($__internal_f58bbf1dc496907ca77e5a1df030a92b4d07495bd5050b23c66d88789a945297_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 2
        echo "
    ";
        // line 3
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "

    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "
        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mr-0\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_homepage");
        echo "\"><span class=\"fa fa-arrow-left fa-2x\"></span></a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\"><span class=\"fa fa-download fa-2x\"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    ";
        // line 38
        $this->displayBlock('ezportfolio_body', $context, $blocks);
        // line 41
        echo "
    ";
        // line 42
        $this->displayBlock('javascripts', $context, $blocks);
        // line 49
        echo "
";
        
        $__internal_f58bbf1dc496907ca77e5a1df030a92b4d07495bd5050b23c66d88789a945297->leave($__internal_f58bbf1dc496907ca77e5a1df030a92b4d07495bd5050b23c66d88789a945297_prof);

        
        $__internal_60cc6bc684e94a7201d8a0232f09f501a4e1544c2cf0766c98eeaa8ca72e3372->leave($__internal_60cc6bc684e94a7201d8a0232f09f501a4e1544c2cf0766c98eeaa8ca72e3372_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_71a66b811432ad9ef337624517d51a3122be1060eef5ec0a89f6b3836f277d58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71a66b811432ad9ef337624517d51a3122be1060eef5ec0a89f6b3836f277d58->enter($__internal_71a66b811432ad9ef337624517d51a3122be1060eef5ec0a89f6b3836f277d58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_fda806e3ba50a72fa85b842f299b225e4e1a2d04fbd2b0f3c9562489e94e0b26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fda806e3ba50a72fa85b842f299b225e4e1a2d04fbd2b0f3c9562489e94e0b26->enter($__internal_fda806e3ba50a72fa85b842f299b225e4e1a2d04fbd2b0f3c9562489e94e0b26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "       Portfolio
    ";
        
        $__internal_fda806e3ba50a72fa85b842f299b225e4e1a2d04fbd2b0f3c9562489e94e0b26->leave($__internal_fda806e3ba50a72fa85b842f299b225e4e1a2d04fbd2b0f3c9562489e94e0b26_prof);

        
        $__internal_71a66b811432ad9ef337624517d51a3122be1060eef5ec0a89f6b3836f277d58->leave($__internal_71a66b811432ad9ef337624517d51a3122be1060eef5ec0a89f6b3836f277d58_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_88a5ebe51c0b508a3bb752a3e10511cb5d7f139710a905b608d00e72403a9f22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_88a5ebe51c0b508a3bb752a3e10511cb5d7f139710a905b608d00e72403a9f22->enter($__internal_88a5ebe51c0b508a3bb752a3e10511cb5d7f139710a905b608d00e72403a9f22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_4f77f8baefffc155a5e43f54095a235c3238a3329561330b84db1facae6e0ccf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f77f8baefffc155a5e43f54095a235c3238a3329561330b84db1facae6e0ccf->enter($__internal_4f77f8baefffc155a5e43f54095a235c3238a3329561330b84db1facae6e0ccf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "

        <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

    ";
        
        $__internal_4f77f8baefffc155a5e43f54095a235c3238a3329561330b84db1facae6e0ccf->leave($__internal_4f77f8baefffc155a5e43f54095a235c3238a3329561330b84db1facae6e0ccf_prof);

        
        $__internal_88a5ebe51c0b508a3bb752a3e10511cb5d7f139710a905b608d00e72403a9f22->leave($__internal_88a5ebe51c0b508a3bb752a3e10511cb5d7f139710a905b608d00e72403a9f22_prof);

    }

    // line 38
    public function block_ezportfolio_body($context, array $blocks = array())
    {
        $__internal_4afac9a742b9c05d8bcde7e69ade6d40cc01ff6939b70a9b487665c01430dac6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4afac9a742b9c05d8bcde7e69ade6d40cc01ff6939b70a9b487665c01430dac6->enter($__internal_4afac9a742b9c05d8bcde7e69ade6d40cc01ff6939b70a9b487665c01430dac6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezportfolio_body"));

        $__internal_a83795b960d2ca2c4fa7c6943bccd0971a24742f53fd33f928522c64b604ee33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a83795b960d2ca2c4fa7c6943bccd0971a24742f53fd33f928522c64b604ee33->enter($__internal_a83795b960d2ca2c4fa7c6943bccd0971a24742f53fd33f928522c64b604ee33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezportfolio_body"));

        // line 39
        echo "
    ";
        
        $__internal_a83795b960d2ca2c4fa7c6943bccd0971a24742f53fd33f928522c64b604ee33->leave($__internal_a83795b960d2ca2c4fa7c6943bccd0971a24742f53fd33f928522c64b604ee33_prof);

        
        $__internal_4afac9a742b9c05d8bcde7e69ade6d40cc01ff6939b70a9b487665c01430dac6->leave($__internal_4afac9a742b9c05d8bcde7e69ade6d40cc01ff6939b70a9b487665c01430dac6_prof);

    }

    // line 42
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_4c0f44fbfad0e1064f5fde05e58628e20ff95a6e634f48ac22da82fa15084dcc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c0f44fbfad0e1064f5fde05e58628e20ff95a6e634f48ac22da82fa15084dcc->enter($__internal_4c0f44fbfad0e1064f5fde05e58628e20ff95a6e634f48ac22da82fa15084dcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_b0dfe3f1ee71d48bc23bde9ba6cca6b4ebaa27dec9d358f888830c51735c475a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0dfe3f1ee71d48bc23bde9ba6cca6b4ebaa27dec9d358f888830c51735c475a->enter($__internal_b0dfe3f1ee71d48bc23bde9ba6cca6b4ebaa27dec9d358f888830c51735c475a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 43
        echo "
        <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/popper/popper.min.js"), "html", null, true);
        echo "\"></script>

    ";
        
        $__internal_b0dfe3f1ee71d48bc23bde9ba6cca6b4ebaa27dec9d358f888830c51735c475a->leave($__internal_b0dfe3f1ee71d48bc23bde9ba6cca6b4ebaa27dec9d358f888830c51735c475a_prof);

        
        $__internal_4c0f44fbfad0e1064f5fde05e58628e20ff95a6e634f48ac22da82fa15084dcc->leave($__internal_4c0f44fbfad0e1064f5fde05e58628e20ff95a6e634f48ac22da82fa15084dcc_prof);

    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle::Layout/portfolio_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  195 => 46,  191 => 45,  187 => 44,  184 => 43,  175 => 42,  164 => 39,  155 => 38,  142 => 13,  138 => 12,  133 => 9,  124 => 8,  113 => 4,  104 => 3,  93 => 49,  91 => 42,  88 => 41,  86 => 38,  71 => 26,  59 => 16,  57 => 8,  53 => 6,  51 => 3,  48 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block body %}

    {% block title %}
       Portfolio
    {% endblock %}


    {% block stylesheets %}


        <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"{{ asset('bundles/webshome/vendor/font-awesome/css/font-awesome.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
        <link href=\"{{ asset('bundles/webshome/vendor/bootstrap/css/bootstrap.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />

    {% endblock %}

        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mr-0\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"{{ path('ez_portfolio_homepage') }}\"><span class=\"fa fa-arrow-left fa-2x\"></span></a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\"><span class=\"fa fa-download fa-2x\"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    {% block ezportfolio_body %}

    {% endblock %}

    {% block javascripts %}

        <script src=\"{{ asset('bundles/webshome/vendor/bootstrap/js/bootstrap.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/webshome/vendor/jquery/jquery.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/webshome/vendor/popper/popper.min.js') }}\"></script>

    {% endblock %}

{% endblock %}", "EZPortfolioBundle::Layout/portfolio_layout.html.twig", "C:\\wamp64\\www\\personal\\EZone\\src\\EZ\\PortfolioBundle/Resources/views/Layout/portfolio_layout.html.twig");
    }
}
