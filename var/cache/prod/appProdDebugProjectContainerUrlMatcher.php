<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/home')) {
            // ez_portfolio_homepage
            if (preg_match('#^/home/(?P<_locale>[^/]++)/?$#s', $pathinfo, $matches)) {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'ez_portfolio_homepage');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ez_portfolio_homepage')), array (  '_controller' => 'EZ\\PortfolioBundle\\Controller\\PortfolioController::indexAction',  '_locale' => 'fr',));
            }

            // ez_portfolio_portfolio
            if (0 === strpos($pathinfo, '/home/portfolio') && preg_match('#^/home/portfolio(?:/(?P<src>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ez_portfolio_portfolio')), array (  '_controller' => 'EZ\\PortfolioBundle\\Controller\\PortfolioController::searchProjectAction',  'src' => 'portfolio',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
