<?php

/* EZPortfolioBundle::Layout/layout.html.twig */
class __TwigTemplate_123ef9cd5b81f76b75cbcab89235a80ba27662d2f387afcbff04c2c31d8fa67a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::template.html.twig", "EZPortfolioBundle::Layout/layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'ezhome_body' => array($this, 'block_ezhome_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb6efc63e6b933359fba76712d035c87aceecfad019300cb95e4bdfde0a3f996 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb6efc63e6b933359fba76712d035c87aceecfad019300cb95e4bdfde0a3f996->enter($__internal_bb6efc63e6b933359fba76712d035c87aceecfad019300cb95e4bdfde0a3f996_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle::Layout/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bb6efc63e6b933359fba76712d035c87aceecfad019300cb95e4bdfde0a3f996->leave($__internal_bb6efc63e6b933359fba76712d035c87aceecfad019300cb95e4bdfde0a3f996_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_48cfe7e909ce61e79c8666c45938c9704c6fd4851d4e87354962204ac422a934 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_48cfe7e909ce61e79c8666c45938c9704c6fd4851d4e87354962204ac422a934->enter($__internal_48cfe7e909ce61e79c8666c45938c9704c6fd4851d4e87354962204ac422a934_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <!-- Header -->
    <header id=\"home\" class=\"intro-header\">
        <div id=\"particles\"></div>
        <div class=\"container\">
            <div class=\"intro-message\">
                <h1>Elie Laloum ,</h1>
                <h3><span class=\"txt-rotate\" data-period=\"2000\" data-rotate='[ \"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("I am a developer."), "html", null, true);
        echo "\", \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("I am a WebDesigner."), "html", null, true);
        echo "\", \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Welcome!."), "html", null, true);
        echo "\" ]'></span></h3>
                <hr class=\"intro-divider\">
                <div class=\"container\">
                    <div class=\"row justify-content-center intro-button\">
                        <div class=\"col-lg-4\">
                            <a href=\"#contact\" class=\"btn btn-secondary btn-lg\">
                                <span class=\"network-name\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Hire Me"), "html", null, true);
        echo "</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    ";
        // line 25
        $this->displayBlock('ezhome_body', $context, $blocks);
        // line 28
        echo "

";
        
        $__internal_48cfe7e909ce61e79c8666c45938c9704c6fd4851d4e87354962204ac422a934->leave($__internal_48cfe7e909ce61e79c8666c45938c9704c6fd4851d4e87354962204ac422a934_prof);

    }

    // line 25
    public function block_ezhome_body($context, array $blocks = array())
    {
        $__internal_517e8614c386aa1d1208072ee6f0bcf57cea31269c8f1b85d0a770164012aba6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_517e8614c386aa1d1208072ee6f0bcf57cea31269c8f1b85d0a770164012aba6->enter($__internal_517e8614c386aa1d1208072ee6f0bcf57cea31269c8f1b85d0a770164012aba6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezhome_body"));

        // line 26
        echo "
    ";
        
        $__internal_517e8614c386aa1d1208072ee6f0bcf57cea31269c8f1b85d0a770164012aba6->leave($__internal_517e8614c386aa1d1208072ee6f0bcf57cea31269c8f1b85d0a770164012aba6_prof);

    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle::Layout/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 26,  85 => 25,  76 => 28,  74 => 25,  62 => 16,  49 => 10,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::template.html.twig\" %}

{% block body %}
    <!-- Header -->
    <header id=\"home\" class=\"intro-header\">
        <div id=\"particles\"></div>
        <div class=\"container\">
            <div class=\"intro-message\">
                <h1>Elie Laloum ,</h1>
                <h3><span class=\"txt-rotate\" data-period=\"2000\" data-rotate='[ \"{{ 'I am a developer.'|trans }}\", \"{{ 'I am a WebDesigner.'|trans }}\", \"{{ 'Welcome!.'|trans }}\" ]'></span></h3>
                <hr class=\"intro-divider\">
                <div class=\"container\">
                    <div class=\"row justify-content-center intro-button\">
                        <div class=\"col-lg-4\">
                            <a href=\"#contact\" class=\"btn btn-secondary btn-lg\">
                                <span class=\"network-name\">{{ 'Hire Me'|trans }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    {% block ezhome_body %}

    {% endblock %}


{% endblock %}", "EZPortfolioBundle::Layout/layout.html.twig", "C:\\wamp64\\www\\EZone\\src\\EZ\\PortfolioBundle/Resources/views/Layout/layout.html.twig");
    }
}
