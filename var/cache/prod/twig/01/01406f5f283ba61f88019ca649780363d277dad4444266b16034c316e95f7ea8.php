<?php

/* EZPortfolioBundle:Portfolio:portfolio_view.html.twig */
class __TwigTemplate_3754c18dfa6a1893a0faa04766971621137752ef13d5868ebd0fa965aa5a3a99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EZPortfolioBundle::Layout/portfolio_layout.html.twig", "EZPortfolioBundle:Portfolio:portfolio_view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'ezportfolio_body' => array($this, 'block_ezportfolio_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EZPortfolioBundle::Layout/portfolio_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c49382677c6b77d30c284abbcd430cd5c3598355e403e677ca091a877dade12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c49382677c6b77d30c284abbcd430cd5c3598355e403e677ca091a877dade12->enter($__internal_2c49382677c6b77d30c284abbcd430cd5c3598355e403e677ca091a877dade12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle:Portfolio:portfolio_view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2c49382677c6b77d30c284abbcd430cd5c3598355e403e677ca091a877dade12->leave($__internal_2c49382677c6b77d30c284abbcd430cd5c3598355e403e677ca091a877dade12_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_73d3ab36122535db0697d17b1a1b50d6574d283d6f156213d08305bf90be845a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73d3ab36122535db0697d17b1a1b50d6574d283d6f156213d08305bf90be845a->enter($__internal_73d3ab36122535db0697d17b1a1b50d6574d283d6f156213d08305bf90be845a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "
    <title>
        ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo " - ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
    </title>

";
        
        $__internal_73d3ab36122535db0697d17b1a1b50d6574d283d6f156213d08305bf90be845a->leave($__internal_73d3ab36122535db0697d17b1a1b50d6574d283d6f156213d08305bf90be845a_prof);

    }

    // line 13
    public function block_ezportfolio_body($context, array $blocks = array())
    {
        $__internal_768b38693d18d51aff5ed825222de0ef589cddc8bcbb9404793b903009ff8069 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_768b38693d18d51aff5ed825222de0ef589cddc8bcbb9404793b903009ff8069->enter($__internal_768b38693d18d51aff5ed825222de0ef589cddc8bcbb9404793b903009ff8069_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezportfolio_body"));

        // line 14
        echo "
    <iframe src=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["src"]) ? $context["src"] : $this->getContext($context, "src")), "html", null, true);
        echo "\" style=\"width: 100%; height: 100vh;\"></iframe>

";
        
        $__internal_768b38693d18d51aff5ed825222de0ef589cddc8bcbb9404793b903009ff8069->leave($__internal_768b38693d18d51aff5ed825222de0ef589cddc8bcbb9404793b903009ff8069_prof);

    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle:Portfolio:portfolio_view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 15,  64 => 14,  58 => 13,  45 => 6,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"EZPortfolioBundle::Layout/portfolio_layout.html.twig\" %}

{% block title %}

    <title>
        {{ name }} - {{ parent() }}
    </title>

{% endblock %}



{% block ezportfolio_body %}

    <iframe src=\"{{ src }}\" style=\"width: 100%; height: 100vh;\"></iframe>

{% endblock %}
", "EZPortfolioBundle:Portfolio:portfolio_view.html.twig", "C:\\wamp64\\www\\EZone\\src\\EZ\\PortfolioBundle\\Resources\\views\\Portfolio\\portfolio_view.html.twig");
    }
}
