<?php

/* EZPortfolioBundle::Layout/portfolio_layout.html.twig */
class __TwigTemplate_08dfd50f3427deb4d69209dc02e55765cd1261db47454bcc9bbe16dc65ec281d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'ezportfolio_body' => array($this, 'block_ezportfolio_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2126082234496e5b2b5fc45df4d7c2449c963acc20d76de722335b07fe42389 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2126082234496e5b2b5fc45df4d7c2449c963acc20d76de722335b07fe42389->enter($__internal_a2126082234496e5b2b5fc45df4d7c2449c963acc20d76de722335b07fe42389_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle::Layout/portfolio_layout.html.twig"));

        // line 1
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_a2126082234496e5b2b5fc45df4d7c2449c963acc20d76de722335b07fe42389->leave($__internal_a2126082234496e5b2b5fc45df4d7c2449c963acc20d76de722335b07fe42389_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_15b36eeb44c56edd177f70abc0449642377328a423982168d16888d91473c4a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15b36eeb44c56edd177f70abc0449642377328a423982168d16888d91473c4a7->enter($__internal_15b36eeb44c56edd177f70abc0449642377328a423982168d16888d91473c4a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 2
        echo "
    ";
        // line 3
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "

    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "
        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mr-0\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_homepage");
        echo "\"><span class=\"fa fa-arrow-left fa-2x\"></span></a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\"><span class=\"fa fa-download fa-2x\"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    ";
        // line 38
        $this->displayBlock('ezportfolio_body', $context, $blocks);
        // line 41
        echo "
    ";
        // line 42
        $this->displayBlock('javascripts', $context, $blocks);
        // line 49
        echo "
";
        
        $__internal_15b36eeb44c56edd177f70abc0449642377328a423982168d16888d91473c4a7->leave($__internal_15b36eeb44c56edd177f70abc0449642377328a423982168d16888d91473c4a7_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_09c8ffd9e5a09675056cb4a29014c8d516e372c119d2e934b2a4534333c09f0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09c8ffd9e5a09675056cb4a29014c8d516e372c119d2e934b2a4534333c09f0c->enter($__internal_09c8ffd9e5a09675056cb4a29014c8d516e372c119d2e934b2a4534333c09f0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "       Portfolio
    ";
        
        $__internal_09c8ffd9e5a09675056cb4a29014c8d516e372c119d2e934b2a4534333c09f0c->leave($__internal_09c8ffd9e5a09675056cb4a29014c8d516e372c119d2e934b2a4534333c09f0c_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9b045f63b84849faafe4f3f66d4f6775c24dbf865aceadf81d70b592317c53af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b045f63b84849faafe4f3f66d4f6775c24dbf865aceadf81d70b592317c53af->enter($__internal_9b045f63b84849faafe4f3f66d4f6775c24dbf865aceadf81d70b592317c53af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "

        <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

    ";
        
        $__internal_9b045f63b84849faafe4f3f66d4f6775c24dbf865aceadf81d70b592317c53af->leave($__internal_9b045f63b84849faafe4f3f66d4f6775c24dbf865aceadf81d70b592317c53af_prof);

    }

    // line 38
    public function block_ezportfolio_body($context, array $blocks = array())
    {
        $__internal_441acd60608b17e4a9a94e8106f0ddd2b3c65faf4d655034d029d3acf708b043 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_441acd60608b17e4a9a94e8106f0ddd2b3c65faf4d655034d029d3acf708b043->enter($__internal_441acd60608b17e4a9a94e8106f0ddd2b3c65faf4d655034d029d3acf708b043_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezportfolio_body"));

        // line 39
        echo "
    ";
        
        $__internal_441acd60608b17e4a9a94e8106f0ddd2b3c65faf4d655034d029d3acf708b043->leave($__internal_441acd60608b17e4a9a94e8106f0ddd2b3c65faf4d655034d029d3acf708b043_prof);

    }

    // line 42
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_6e75a8cc22da61735b58b6a11fe4511f61539a4b34067cb6682eb536f99b5b36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e75a8cc22da61735b58b6a11fe4511f61539a4b34067cb6682eb536f99b5b36->enter($__internal_6e75a8cc22da61735b58b6a11fe4511f61539a4b34067cb6682eb536f99b5b36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 43
        echo "
        <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/popper/popper.min.js"), "html", null, true);
        echo "\"></script>

    ";
        
        $__internal_6e75a8cc22da61735b58b6a11fe4511f61539a4b34067cb6682eb536f99b5b36->leave($__internal_6e75a8cc22da61735b58b6a11fe4511f61539a4b34067cb6682eb536f99b5b36_prof);

    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle::Layout/portfolio_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  162 => 46,  158 => 45,  154 => 44,  151 => 43,  145 => 42,  137 => 39,  131 => 38,  121 => 13,  117 => 12,  112 => 9,  106 => 8,  98 => 4,  92 => 3,  84 => 49,  82 => 42,  79 => 41,  77 => 38,  62 => 26,  50 => 16,  48 => 8,  44 => 6,  42 => 3,  39 => 2,  27 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block body %}

    {% block title %}
       Portfolio
    {% endblock %}


    {% block stylesheets %}


        <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"{{ asset('bundles/webshome/vendor/font-awesome/css/font-awesome.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
        <link href=\"{{ asset('bundles/webshome/vendor/bootstrap/css/bootstrap.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />

    {% endblock %}

        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mr-0\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"{{ path('ez_portfolio_homepage') }}\"><span class=\"fa fa-arrow-left fa-2x\"></span></a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\"><span class=\"fa fa-download fa-2x\"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    {% block ezportfolio_body %}

    {% endblock %}

    {% block javascripts %}

        <script src=\"{{ asset('bundles/webshome/vendor/bootstrap/js/bootstrap.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/webshome/vendor/jquery/jquery.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/webshome/vendor/popper/popper.min.js') }}\"></script>

    {% endblock %}

{% endblock %}", "EZPortfolioBundle::Layout/portfolio_layout.html.twig", "C:\\wamp64\\www\\EZone\\src\\EZ\\PortfolioBundle/Resources/views/Layout/portfolio_layout.html.twig");
    }
}
