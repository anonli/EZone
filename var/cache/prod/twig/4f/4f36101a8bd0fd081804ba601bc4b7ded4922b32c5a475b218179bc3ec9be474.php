<?php

/* @EZPortfolio/Layout/layout.html.twig */
class __TwigTemplate_9b7d78bfe4f6ace1db26e0db0e908cfbf9605388284f91983a0c116d534d0282 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::template.html.twig", "@EZPortfolio/Layout/layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'ezhome_body' => array($this, 'block_ezhome_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <!-- Header -->
    <header id=\"home\" class=\"intro-header\">
        <div id=\"particles\"></div>
        <div class=\"container\">
            <div class=\"intro-message\">
                <h1>Elie Laloum ,</h1>
                <h3><span class=\"txt-rotate\" data-period=\"2000\" data-rotate='[ \"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("I am a developer."), "html", null, true);
        echo "\", \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("I am a WebDesigner."), "html", null, true);
        echo "\", \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Welcome!."), "html", null, true);
        echo "\" ]'></span></h3>
                <hr class=\"intro-divider\">
                <div class=\"container\">
                    <div class=\"row justify-content-center intro-button\">
                        <div class=\"col-lg-4\">
                            <a href=\"#contact\" class=\"btn btn-secondary btn-lg\">
                                <span class=\"network-name\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Hire Me"), "html", null, true);
        echo "</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    ";
        // line 25
        $this->displayBlock('ezhome_body', $context, $blocks);
        // line 28
        echo "

";
    }

    // line 25
    public function block_ezhome_body($context, array $blocks = array())
    {
        // line 26
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "@EZPortfolio/Layout/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 26,  73 => 25,  67 => 28,  65 => 25,  53 => 16,  40 => 10,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@EZPortfolio/Layout/layout.html.twig", "C:\\wamp64\\www\\EZone\\src\\EZ\\PortfolioBundle\\Resources\\views\\Layout\\layout.html.twig");
    }
}
