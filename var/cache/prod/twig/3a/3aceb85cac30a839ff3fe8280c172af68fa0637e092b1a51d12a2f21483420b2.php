<?php

/* EZPortfolioBundle::Layout/layout.html.twig */
class __TwigTemplate_71ed896dc067debe0b199319dc998e89f9bf40d3fc085285707d63838f860ae1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::template.html.twig", "EZPortfolioBundle::Layout/layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'ezhome_body' => array($this, 'block_ezhome_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8be8dd3b2220151a549c58f78ece2d5a7010a8f4166146c5685e546053d6e1bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8be8dd3b2220151a549c58f78ece2d5a7010a8f4166146c5685e546053d6e1bc->enter($__internal_8be8dd3b2220151a549c58f78ece2d5a7010a8f4166146c5685e546053d6e1bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EZPortfolioBundle::Layout/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8be8dd3b2220151a549c58f78ece2d5a7010a8f4166146c5685e546053d6e1bc->leave($__internal_8be8dd3b2220151a549c58f78ece2d5a7010a8f4166146c5685e546053d6e1bc_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_fab935916f0f416969f7ad3a2828477a618eadcc82201df505d6b26a8c707dc3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fab935916f0f416969f7ad3a2828477a618eadcc82201df505d6b26a8c707dc3->enter($__internal_fab935916f0f416969f7ad3a2828477a618eadcc82201df505d6b26a8c707dc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <!-- Header -->
    <header id=\"home\" class=\"intro-header\">
        <div id=\"particles\"></div>
        <div class=\"container\">
            <div class=\"intro-message\">
                <h1>Elie Laloum ,</h1>
                <h3><span class=\"txt-rotate\" data-period=\"2000\" data-rotate='[ \"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("I am a developer."), "html", null, true);
        echo "\", \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("I am a WebDesigner."), "html", null, true);
        echo "\", \"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Welcome!."), "html", null, true);
        echo "\" ]'></span></h3>
                <hr class=\"intro-divider\">
                <div class=\"container\">
                    <div class=\"row justify-content-center intro-button\">
                        <div class=\"col-lg-4\">
                            <a href=\"#contact\" class=\"btn btn-secondary btn-lg\">
                                <span class=\"network-name\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Hire Me"), "html", null, true);
        echo "</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    ";
        // line 25
        $this->displayBlock('ezhome_body', $context, $blocks);
        // line 28
        echo "

";
        
        $__internal_fab935916f0f416969f7ad3a2828477a618eadcc82201df505d6b26a8c707dc3->leave($__internal_fab935916f0f416969f7ad3a2828477a618eadcc82201df505d6b26a8c707dc3_prof);

    }

    // line 25
    public function block_ezhome_body($context, array $blocks = array())
    {
        $__internal_bc49dba1405ea0714bbef15db5b02bebb08e5af5c2c242b33a0e476fbdd5a7bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc49dba1405ea0714bbef15db5b02bebb08e5af5c2c242b33a0e476fbdd5a7bb->enter($__internal_bc49dba1405ea0714bbef15db5b02bebb08e5af5c2c242b33a0e476fbdd5a7bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ezhome_body"));

        // line 26
        echo "
    ";
        
        $__internal_bc49dba1405ea0714bbef15db5b02bebb08e5af5c2c242b33a0e476fbdd5a7bb->leave($__internal_bc49dba1405ea0714bbef15db5b02bebb08e5af5c2c242b33a0e476fbdd5a7bb_prof);

    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle::Layout/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 26,  85 => 25,  76 => 28,  74 => 25,  62 => 16,  49 => 10,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::template.html.twig\" %}

{% block body %}
    <!-- Header -->
    <header id=\"home\" class=\"intro-header\">
        <div id=\"particles\"></div>
        <div class=\"container\">
            <div class=\"intro-message\">
                <h1>Elie Laloum ,</h1>
                <h3><span class=\"txt-rotate\" data-period=\"2000\" data-rotate='[ \"{{ 'I am a developer.'|trans }}\", \"{{ 'I am a WebDesigner.'|trans }}\", \"{{ 'Welcome!.'|trans }}\" ]'></span></h3>
                <hr class=\"intro-divider\">
                <div class=\"container\">
                    <div class=\"row justify-content-center intro-button\">
                        <div class=\"col-lg-4\">
                            <a href=\"#contact\" class=\"btn btn-secondary btn-lg\">
                                <span class=\"network-name\">{{ 'Hire Me'|trans }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    {% block ezhome_body %}

    {% endblock %}


{% endblock %}", "EZPortfolioBundle::Layout/layout.html.twig", "C:\\wamp64\\www\\EZone\\src\\EZ\\PortfolioBundle/Resources/views/Layout/layout.html.twig");
    }
}
