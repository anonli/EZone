<?php

/* @EZPortfolio/Layout/portfolio_layout.html.twig */
class __TwigTemplate_90638c204a7884fad802f21d12ffc54e4a9caff2f54305112a7a01f84f23096f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'ezportfolio_body' => array($this, 'block_ezportfolio_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('body', $context, $blocks);
    }

    public function block_body($context, array $blocks = array())
    {
        // line 2
        echo "
    ";
        // line 3
        $this->displayBlock('title', $context, $blocks);
        // line 6
        echo "

    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "
        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mr-0\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_homepage");
        echo "\"><span class=\"fa fa-arrow-left fa-2x\"></span></a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\"><span class=\"fa fa-download fa-2x\"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    ";
        // line 38
        $this->displayBlock('ezportfolio_body', $context, $blocks);
        // line 41
        echo "
    ";
        // line 42
        $this->displayBlock('javascripts', $context, $blocks);
        // line 49
        echo "
";
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "       Portfolio
    ";
    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 9
        echo "

        <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

    ";
    }

    // line 38
    public function block_ezportfolio_body($context, array $blocks = array())
    {
        // line 39
        echo "
    ";
    }

    // line 42
    public function block_javascripts($context, array $blocks = array())
    {
        // line 43
        echo "
        <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/popper/popper.min.js"), "html", null, true);
        echo "\"></script>

    ";
    }

    public function getTemplateName()
    {
        return "@EZPortfolio/Layout/portfolio_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  129 => 46,  125 => 45,  121 => 44,  118 => 43,  115 => 42,  110 => 39,  107 => 38,  100 => 13,  96 => 12,  91 => 9,  88 => 8,  83 => 4,  80 => 3,  75 => 49,  73 => 42,  70 => 41,  68 => 38,  53 => 26,  41 => 16,  39 => 8,  35 => 6,  33 => 3,  30 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@EZPortfolio/Layout/portfolio_layout.html.twig", "C:\\wamp64\\www\\EZone\\src\\EZ\\PortfolioBundle\\Resources\\views\\Layout\\portfolio_layout.html.twig");
    }
}
