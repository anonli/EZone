<?php

/* ::template.html.twig */
class __TwigTemplate_b87f5622548b907bcb2ea9f658ea895129cb7b5591378b0530a87b2e06c19151 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_490d06ac5b9799454d81d1421fcd88fceb68a218b8514735847fed94aa2feba0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_490d06ac5b9799454d81d1421fcd88fceb68a218b8514735847fed94aa2feba0->enter($__internal_490d06ac5b9799454d81d1421fcd88fceb68a218b8514735847fed94aa2feba0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::template.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/html\">

    <head>

        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        ";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        // line 12
        echo "
        ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 23
        echo "
    </head>

        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#home\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("HOME"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ABOUT"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#services\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SERVICES"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#works\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("WORKS"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#contact\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("CONTACT"), "html", null, true);
        echo "</a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mr-0>
                        <li class=\"nav-item\">
                            <a href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_homepage", array("_locale" => "fr"));
        echo "\" class=\"nav-link\"><img class=\"mt-1\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/img/french.png"), "html", null, true);
        echo "\"></img></a>
                        </li>
                        <li class=\"nav-item mr-5\">
                            <a href=\"";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_homepage", array("_locale" => "en"));
        echo "\" class=\"nav-link\"><img class=\"mt-1\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/img/england.png"), "html", null, true);
        echo "\"></img></a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        ";
        // line 63
        $this->displayBlock('body', $context, $blocks);
        // line 66
        echo "
        ";
        // line 67
        $this->displayBlock('javascripts', $context, $blocks);
        // line 80
        echo "
    </body>

</html>";
        
        $__internal_490d06ac5b9799454d81d1421fcd88fceb68a218b8514735847fed94aa2feba0->leave($__internal_490d06ac5b9799454d81d1421fcd88fceb68a218b8514735847fed94aa2feba0_prof);

    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        $__internal_1ad31ff8d0fbfc5981f91d54a07de6518750f9a825df4c8a49f0e6d5fce2087d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ad31ff8d0fbfc5981f91d54a07de6518750f9a825df4c8a49f0e6d5fce2087d->enter($__internal_1ad31ff8d0fbfc5981f91d54a07de6518750f9a825df4c8a49f0e6d5fce2087d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 10
        echo "            Portfolio
        ";
        
        $__internal_1ad31ff8d0fbfc5981f91d54a07de6518750f9a825df4c8a49f0e6d5fce2087d->leave($__internal_1ad31ff8d0fbfc5981f91d54a07de6518750f9a825df4c8a49f0e6d5fce2087d_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b4ccdf8a0dff95a669b89e810da960dffb1188eb43654f0f0e853562888468b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4ccdf8a0dff95a669b89e810da960dffb1188eb43654f0f0e853562888468b4->enter($__internal_b4ccdf8a0dff95a669b89e810da960dffb1188eb43654f0f0e853562888468b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "

            <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">

            <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/css/landing-page.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

        ";
        
        $__internal_b4ccdf8a0dff95a669b89e810da960dffb1188eb43654f0f0e853562888468b4->leave($__internal_b4ccdf8a0dff95a669b89e810da960dffb1188eb43654f0f0e853562888468b4_prof);

    }

    // line 63
    public function block_body($context, array $blocks = array())
    {
        $__internal_ca83d4ffc0357f6cf7cbc480f456cdb4ae2856d9bec86069ee769f73d7e7b884 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca83d4ffc0357f6cf7cbc480f456cdb4ae2856d9bec86069ee769f73d7e7b884->enter($__internal_ca83d4ffc0357f6cf7cbc480f456cdb4ae2856d9bec86069ee769f73d7e7b884_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 64
        echo "
        ";
        
        $__internal_ca83d4ffc0357f6cf7cbc480f456cdb4ae2856d9bec86069ee769f73d7e7b884->leave($__internal_ca83d4ffc0357f6cf7cbc480f456cdb4ae2856d9bec86069ee769f73d7e7b884_prof);

    }

    // line 67
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_98fce692915526b788d0e122b6908e6d0ef39cb30a943f2bce45f70ea96a914c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98fce692915526b788d0e122b6908e6d0ef39cb30a943f2bce45f70ea96a914c->enter($__internal_98fce692915526b788d0e122b6908e6d0ef39cb30a943f2bce45f70ea96a914c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 68
        echo "
            <!-- Script -->
            <script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/popper/popper.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/vendor/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

            <script src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/particle.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/skillanim.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/main.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/js/txtRotate.js"), "html", null, true);
        echo "\"></script>

        ";
        
        $__internal_98fce692915526b788d0e122b6908e6d0ef39cb30a943f2bce45f70ea96a914c->leave($__internal_98fce692915526b788d0e122b6908e6d0ef39cb30a943f2bce45f70ea96a914c_prof);

    }

    public function getTemplateName()
    {
        return "::template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 77,  216 => 76,  212 => 75,  208 => 74,  203 => 72,  199 => 71,  195 => 70,  191 => 68,  185 => 67,  177 => 64,  171 => 63,  161 => 20,  157 => 19,  153 => 18,  147 => 14,  141 => 13,  133 => 10,  127 => 9,  117 => 80,  115 => 67,  112 => 66,  110 => 63,  97 => 55,  89 => 52,  81 => 47,  75 => 44,  69 => 41,  63 => 38,  57 => 35,  43 => 23,  41 => 13,  38 => 12,  36 => 9,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/html\">

    <head>

        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        {% block title %}
            Portfolio
        {% endblock %}

        {% block stylesheets %}


            <link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">

            <link href=\"{{ asset('bundles/webshome/css/landing-page.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"{{ asset('bundles/webshome/vendor/font-awesome/css/font-awesome.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"{{ asset('bundles/webshome/vendor/bootstrap/css/bootstrap.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />

        {% endblock %}

    </head>

        <!-- Navigation -->
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light fixed-top\">
            <div class=\"container-fluid\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                    <ul class=\"navbar-nav mx-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#home\">{{ 'HOME'|trans }}</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#about\">{{ 'ABOUT'|trans }}</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#services\">{{ 'SERVICES'|trans }}</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#works\">{{ 'WORKS'|trans }}</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"#contact\">{{ 'CONTACT'|trans }}</a>
                        </li>
                    </ul>
                    <ul class=\"navbar-nav mr-0>
                        <li class=\"nav-item\">
                            <a href=\"{{ path('ez_portfolio_homepage', {'_locale': 'fr'}) }}\" class=\"nav-link\"><img class=\"mt-1\" src=\"{{ asset('bundles/webshome/img/french.png') }}\"></img></a>
                        </li>
                        <li class=\"nav-item mr-5\">
                            <a href=\"{{ path('ez_portfolio_homepage', {'_locale': 'en'}) }}\" class=\"nav-link\"><img class=\"mt-1\" src=\"{{ asset('bundles/webshome/img/england.png') }}\"></img></a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        {% block body %}

        {% endblock %}

        {% block javascripts %}

            <!-- Script -->
            <script src=\"{{ asset('bundles/webshome/vendor/jquery/jquery.min.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/vendor/popper/popper.min.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/vendor/bootstrap/js/bootstrap.min.js') }}\"></script>

            <script src=\"{{ asset('bundles/webshome/js/particle.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/js/skillanim.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/js/main.js') }}\"></script>
            <script src=\"{{ asset('bundles/webshome/js/txtRotate.js') }}\"></script>

        {% endblock %}

    </body>

</html>", "::template.html.twig", "C:\\wamp64\\www\\EZone\\app\\Resources\\views\\template.html.twig");
    }
}
