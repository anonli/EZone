<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_efa2143a4dc2d6290045f496d4c8309944451f96f1aa7e1c864bcebc34020b57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_82aa30779d02a71f23745ad6eb79c48a9d9edaceb5ac2f52412580a9c890da35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82aa30779d02a71f23745ad6eb79c48a9d9edaceb5ac2f52412580a9c890da35->enter($__internal_82aa30779d02a71f23745ad6eb79c48a9d9edaceb5ac2f52412580a9c890da35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_82aa30779d02a71f23745ad6eb79c48a9d9edaceb5ac2f52412580a9c890da35->leave($__internal_82aa30779d02a71f23745ad6eb79c48a9d9edaceb5ac2f52412580a9c890da35_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "C:\\wamp64\\www\\EZone\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\chevron-right.svg");
    }
}
