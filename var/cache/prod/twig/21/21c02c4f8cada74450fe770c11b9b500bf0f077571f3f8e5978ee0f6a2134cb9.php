<?php

/* EZPortfolioBundle:Portfolio:index.html.twig */
class __TwigTemplate_7177fade65db92a4081a1d47cd594934cfea18afefb89c4e051f15ee911da537 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EZPortfolioBundle::Layout/layout.html.twig", "EZPortfolioBundle:Portfolio:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'ezhome_body' => array($this, 'block_ezhome_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EZPortfolioBundle::Layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "
    <title>
    Home - ";
        // line 6
        $this->displayParentBlock("title", $context, $blocks);
        echo "
    </title>

";
    }

    // line 11
    public function block_ezhome_body($context, array $blocks = array())
    {
        // line 12
        echo "
    <!-- Page Content -->
    <section id=\"about\" class=\"content-section-a\">

        <div class=\"container\">
            <div class=\"row\">

                <div class=\"col-lg-5 mr-auto overlayrepere\">
                    <img class=\"img-about img-fluid\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/webshome/img/default_avatar.jpg"), "html", null, true);
        echo "\">
                    <div class=\"overlay\">
                        <div class=\"row\">
                            <div class=\"col-lg-12 text-center\">
                                <a href=\"#\" class=\"fa fa-facebook-square fa-3x\"></a>
                                <a href=\"#\" class=\"fa fa-twitter-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-linkedin-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-github-square fa-3x ml-1\"></a>
                                <a href=\"#\" class=\"fa fa-google-plus-square  fa-3x ml-1\"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"smallScreenSpace col-lg-5 ml-auto\">
                    <hr class=\"section-heading-spacer\">
                    <div class=\"clearfix\"></div>
                    <h2 class=\"section-heading\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ABOUT ME"), "html", null, true);
        echo "</h2>
                    <p class=\"lead\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Graduate from the A-Level STI2D option SIN and Senior technician patent Computer services to the organizations, I am currently doing a Licence Analyst & Computer Engineering."), "html", null, true);
        echo "<br><br>
                        ";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Passionate about computer science and technology in general I am fascinated by the understanding of the different systems around us."), "html", null, true);
        echo "<br><br>
                        ";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Ambitious and motivated, I am actively looking for an alternative training that will allow me to deploy my skills and talents within a professional infrastructure."), "html", null, true);
        echo "</p>
                </div>

            </div>
        </div>

    </section>

    <section class=\"content-section-c\">

        <div id=\"skills\" class=\"container-fluid\">

            <div class=\"row justify-content-center\">

                <div class=\"col-lg-6 section-skills align-self-center\">

                    <div class=\"container\">

                        <div class=\"row justify-content-center\">

                            <div class=\"col-lg-9\">

                                <h2>";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("MY SKILLS"), "html", null, true);
        echo "</h2><br>
                                <h4>";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SERIOUS SKILLS BUT KEEP LEARNING"), "html", null, true);
        echo "</h4>
                                <p>";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("In 3 years of learning I acquired a lot of knowledge that already allows me to meet the expectations of a project."), "html", null, true);
        echo "</p>

                                <a href=\"#works\">
                                    <span class=\"network-name\">";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SEE MY WORKS"), "html", null, true);
        echo "</span>
                                </a>

                            </div>

                        </div>

                    </div>

                </div>

                <div class=\"col-lg-1\"></div>

                <div class=\"col-lg-4 mr-auto order-lg-2 justify-content-center align-self-center\">

                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("WEB DEV"), "html", null, true);
        echo "</p>
                        <div class=\"skillbar clearfix \" data-percent=\"90%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">90%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("WEBDESIGN"), "html", null, true);
        echo "</p>
                        <div class=\"skillbar clearfix \" data-percent=\"70%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">70%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SOFTWARE DEV"), "html", null, true);
        echo "</p>
                        <div class=\"skillbar clearfix \" data-percent=\"60%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">60%</div>
                        </div>
                    </div>
                    <div class=\"container skillcontainer\">
                        <p class=\"lead skilltext\">";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("APP DEV"), "html", null, true);
        echo "</p>
                        <div class=\"skillbar clearfix \" data-percent=\"50%\">
                            <div class=\"skillbar-bar\"></div>
                            <div class=\"skill-bar-percent\">50%</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id=\"services\" class=\"content-section-a text-center\">
        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("SERVICES I OFFER"), "html", null, true);
        echo "</h2>

                    <div class=\"row service-row justify-content-center\">

                        <div class=\"col-lg-3 col-sm-6 col-md-6 col-xs-6 mt-4\">

                            <a class=\"service-link\">
                                <div id=\"st1\" class=\"service-thumb\">

                                    <i class=\"fa fa-wordpress fa-4x\"></i>

                                    <h3>CMS</h3>
                                    <p class=\"lead\">";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("For a project that can be modularized and stylised with fast handling."), "html", null, true);
        echo "</p>
                                </div>
                            </a>

                        </div>

                        <div class=\"col-lg-3 col-sm-6 col-md-6 col-xs-6 mt-4\">

                            <a class=\"service-link\">
                                <div id=\"st2\" class=\"service-thumb\">

                                    <i class=\"faicon fa fa-paint-brush  fa-4x\"></i>

                                    <h3>WEBDESIGN</h3>
                                    <p class=\"lead\">";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Template styliser according to specifications."), "html", null, true);
        echo "</p>
                                </div>
                            </a>

                        </div>

                        <div class=\"col-lg-3 col-sm-12 col-md-12 col-xs-12 mt-4\">

                            <a class=\"service-link\">
                                <div id=\"st3\" class=\"service-thumb\">

                                    <i class=\"fa fa-code fa-4x\"></i>

                                    <h3>";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("DEVELOPEMENT"), "html", null, true);
        echo "</h3>
                                    <p class=\"lead\">";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Heavy client development, mobile app or web service."), "html", null, true);
        echo "</p>
                                </div>
                            </a>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id=\"works\" class=\"content-section-b text-center\">
        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("MY WORKS"), "html", null, true);
        echo "</h2>
                    <br>

                    <div class=\"row justify-content-center\">

                        <div class=\"col-lg-12\">
                            <button class=\"tagBtn\" onclick=\"showTag('personal'); hideTag('school'); hideTag('professional')\">";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Personal"), "html", null, true);
        echo "</button>
                            <button class=\"tagBtn\" onclick=\"showTag('school'); hideTag('personal'); hideTag('professional')\">";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("School"), "html", null, true);
        echo "</button>
                            <button class=\"tagBtn\" onclick=\"showTag('professional'); hideTag('personal'); hideTag('school')\">";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Professional"), "html", null, true);
        echo "</button>
                        </div>

                        <div style=\"\" class=\"col-lg-8 mt-5 personal\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 195
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "scplateform"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">SC Plateform</h5>
                                        <p class=\"col-lg-12\">";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("No describe for now."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 203
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "customlocalhost"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">";
        // line 205
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Custom localhost"), "html", null, true);
        echo "</h5>
                                        <p class=\"col-lg-12\">";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("HTML/CSS/JS System for custom localhost ( listing projects , custom links )."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                        <div style=\"display: none;\" class=\"col-lg-8 mt-5 professional\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 219
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "guesswhat"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">GuessWhat</h5>
                                        <p class=\"col-lg-12\">";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Development of a mobile comparator in HTML/CSS/PHP."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 227
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "igaboho"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"";
        // line 228
        echo "http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">Iga Boho Chic</h5>
                                        <p class=\"col-lg-12\">";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Showcase site for an infrastructure of jewelry sales and seasonal clothing."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                        <div style=\"display: none;\" class=\"col-lg-8 mt-5 school\">

                            <div class=\"row\">

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 243
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "ppe1"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">PPE 1</h5>
                                        <p class=\"col-lg-12\">";
        // line 246
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Diploma Validation Project."), "html", null, true);
        echo "</p>
                                        <p class=\"col-lg-12\">";
        // line 247
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Web client for customers of a car rental company."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                                <div class=\"col-lg-3 col-md-4 col-xs-4 col-sm-4 portfolioThumb\">
                                    <a href=\"";
        // line 252
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ez_portfolio_portfolio", array("src" => "ppe2"));
        echo "\" class=\"d-block mb-4 h-100\">
                                        <img class=\"img-fluid img-thumbnail imgThumb\" src=\"http://placehold.it/400x300\" alt=\"\">
                                        <h5 class=\"mt-4 col-lg-12\">PPE 2</h5>
                                        <p class=\"col-lg-12\">";
        // line 255
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Diploma Validation Project."), "html", null, true);
        echo "</p>
                                        <p class=\"col -lg-12\">";
        // line 256
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Heavy client for the management of the car rental company database."), "html", null, true);
        echo "</p>
                                    </a>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id=\"experiences\" class=\"content-section-a text-center\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    <h2>";
        // line 276
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("MY EXPERIENCE"), "html", null, true);
        echo "</h2>

                    <div class=\"row\">

                        <div class=\"frame col-lg-12\">

                            <!-- SMALL SCREEN -->

                            <div class=\"row justify-content-center ExpSmall\">
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-5\">
                                    <div class=\"row\">
                                        <div class=\"col-4 logopart\">
                                            <i class=\"mt-4 fa fa-code fa-3x\" aria-hidden=\"true\"></i>
                                        </div>
                                        <div class=\"descpart col-8 text-left\">
                                            <ul class=\"mt-1\">
                                                <li>GUESSWHAT</li>
                                                <li>2017</li>
                                            </ul>
                                            <p>Mauris balqnuet lorem ipsus dolores minum aerum tanges.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 2</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 3</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 4</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 5</p>
                                </div>
                                <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-3\">
                                    <p>text experience 6</p>
                                </div>
                            </div>

                            <!-- COMPUTER SCREEN -->

                            <div class=\"row mt-5\">

                                <!-- LEFT -->

                                <div class=\"lframe col-lg-6\">

                                    <div class=\"row\">

                                        <div class=\"thumb col-lg-8 mr-0 ml-auto\">

                                            <div class=\"row\">

                                                <!-- Icon & Details -->
                                                <div class=\"col-lg-4 lpart\">

                                                    <div class=\"col-lg-12\">
                                                        <i class=\"mt-4 fa fa-code fa-3x\" aria-hidden=\"true\"></i>
                                                    </div>

                                                    <div class=\"col-lg-12 mt-2 pl-0 pr-0\">
                                                        <p>IGABOHO</p>
                                                    </div>

                                                </div>

                                                <!-- Desc -->
                                                <div class=\"col-lg-8 rpart text-left\">

                                                    <ul class=\"mt-3\">
                                                        <li>";
        // line 347
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Showcase site dev."), "html", null, true);
        echo "</li>
                                                        <li>PRESTASHOP.</li>
                                                        <li>";
        // line 349
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("CSS Templating."), "html", null, true);
        echo "</li>
                                                    </ul>

                                                </div>

                                            </div>

                                        </div>

                                        <div class=\"ythumb col-1 text-right\">
                                            <p>2017</p>
                                        </div>
                                        <div class=\"col-1\"></div>

                                    </div>

                                    <div class=\"row mt-4\">

                                        <div class=\"thumb col-lg-8 mr-0 ml-auto\">

                                            <div class=\"row\">

                                                <!-- Icon & Details -->
                                                <div class=\"col-lg-4 lpart\">

                                                    <div class=\"col-lg-12\">
                                                        <i class=\"mt-4 fa fa-graduation-cap fa-3x\" aria-hidden=\"true\"></i>
                                                    </div>

                                                    <div class=\"col-lg-12 mt-2 pl-0 pr-0\">
                                                        <p>CFA INSTA</p>
                                                    </div>

                                                </div>

                                                <!-- Desc -->
                                                <div class=\"col-lg-8 rpart text-left\">

                                                    <ul class=\"mt-3\">
                                                        <li>";
        // line 388
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("12th Grade."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 389
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("IT Service to Organizations."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 390
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Software solutions and business applications."), "html", null, true);
        echo "</li>
                                                    </ul>

                                                </div>

                                            </div>

                                        </div>

                                        <div class=\"ythumb col-1 text-right\">
                                            <p>2017</p>
                                        </div>
                                        <div class=\"col-1\"></div>

                                    </div>

                                    <div class=\"row mt-4\">

                                        <div class=\"thumb col-lg-8 mr-0 ml-auto\">

                                            <div class=\"row\">

                                                <!-- Icon & Details -->
                                                <div class=\"col-lg-4 lpart\">

                                                    <div class=\"col-lg-12\">
                                                        <i class=\"mt-4 fa fa-code fa-3x\" aria-hidden=\"true\"></i>
                                                    </div>

                                                    <div class=\"col-lg-12 mt-2 pl-0 pr-0\">
                                                        <p>GUESSWHAT</p>
                                                    </div>

                                                </div>

                                                <!-- Desc -->
                                                <div class=\"col-lg-8 rpart text-left\">

                                                    <ul class=\"mt-3\">
                                                        <li>";
        // line 429
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Web developement."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 430
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Mobile suscription comparator."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 431
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Integration to another website."), "html", null, true);
        echo "</li>
                                                    </ul>

                                                </div>

                                            </div>

                                        </div>

                                        <div class=\"ythumb col-1 text-right\">
                                            <p>2015</p>
                                        </div>
                                        <div class=\"col-1\"></div>

                                    </div>

                                </div>

                                <!-- RIGHT -->

                                <div class=\"rframe col-lg-6\">

                                    <div class=\"row\">

                                        <div class=\"col-1\"></div>
                                        <div class=\"ythumb col-1 text-right\">
                                            <p>2017</p>
                                        </div>

                                        <div class=\"thumb col-lg-8\">

                                            <div class=\"row\">

                                                <!-- Desc -->
                                                <div class=\"col-lg-8 lpart text-left\">

                                                    <ul class=\"mt-3\">
                                                        <li>Licence</li>
                                                        <li>";
        // line 469
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Analyst & Computer Engineering."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 470
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Developement."), "html", null, true);
        echo "</li>
                                                    </ul>

                                                </div>

                                                <!-- Icon & Details -->
                                                <div class=\"col-lg-4 rpart\">
                                                    <div class=\"col-lg-12\">
                                                        <i class=\"mt-4 fa fa-graduation-cap fa-3x\" aria-hidden=\"true\"></i>
                                                    </div>

                                                    <div class=\"col-lg-12 mt-2 pl-0 pr-0\">
                                                        <p>CFA INSTA</p>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class=\"row mt-4\">

                                        <div class=\"col-1\"></div>
                                        <div class=\"ythumb col-1 text-center\">
                                            <p>2016</p>
                                        </div>

                                        <div class=\"thumb col-lg-8\">

                                            <div class=\"row\">

                                                <!-- Desc -->
                                                <div class=\"col-lg-8 lpart text-left\">

                                                    <ul class=\"textpart mt-3\">
                                                        <li>";
        // line 507
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Web service dev for rent car company."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 508
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Heavy client to manage db."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 509
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Mobile app for clients."), "html", null, true);
        echo "</li>
                                                    </ul>

                                                </div>

                                                <!-- Icon & Details -->
                                                <div class=\"col-lg-4 rpart\">
                                                    <div class=\"col-lg-12\">
                                                        <i class=\"mt-4 fa fa-code fa-3x\" aria-hidden=\"true\"></i>
                                                    </div>

                                                    <div class=\"col-lg-12 mt-2 pl-0 pr-0\">
                                                        <p>PPEs</p>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class=\"row mt-4\">

                                        <div class=\"col-1\"></div>
                                        <div class=\"ythumb col-1 text-right\">
                                            <p>2014</p>
                                        </div>

                                        <div class=\"thumb col-lg-8\">

                                            <div class=\"row\">

                                                <!-- Desc -->
                                                <div class=\"col-lg-8 lpart text-left\">

                                                    <ul class=\"mt-3\">
                                                        <li>";
        // line 546
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("High-School Degree."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 547
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Science and Technology for Industry and Sustainable Development."), "html", null, true);
        echo "</li>
                                                        <li>";
        // line 548
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Option Science Industrial & Digital."), "html", null, true);
        echo "</li>
                                                    </ul>

                                                </div>

                                                <!-- Icon & Details -->
                                                <div class=\"col-lg-4 rpart\">
                                                    <div class=\"col-lg-12\">
                                                        <i class=\"mt-4 fa fa-graduation-cap fa-3x\" aria-hidden=\"true\"></i>
                                                    </div>

                                                    <div class=\"col-lg-12 mt-2 pl-0 pr-0\">
                                                        <p>JEAN PERRIN</p>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <hr>
        </div>
    </section>

    <section id=\"contact\" class=\"content-section-c text-center\">

        <div class=\"container-fluid\">

            <div class=\"row\">

                <div class=\"col-lg-12 section-contact\">
                    <h2>";
        // line 591
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("CONTACT ME"), "html", null, true);
        echo "</h2>

                    <div class=\"row\">

                        <div class=\"col-lg-3\"></div>

                        <div class=\"col-lg-6 mt-5\">
                            <div class=\"row justify-content-center\">
                                <div class=\"frame col-lg-8\">
                                    <form>
                                        <div class=\"form-group\">
                                            <div class=\"row mt-5 justify-content-center\">
                                                <input type=\"email\" class=\"emailInput mt-1 col-lg-5 col-md-4 col-xs-4 col-sm-4 col-6 form-control\" id=\"exampleFormControlInput1\" placeholder=\"";
        // line 603
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("name@example.com"), "html", null, true);
        echo "\">
                                                <div class=\"col-1\"></div>
                                                <input type=\"object\" class=\"objectInput mt-1 col-lg-5 col-md-4 col-xs-4 col-sm-4 col-6 form-control\" id=\"exampleInputEmail1\" placeholder=\"";
        // line 605
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Enter subject"), "html", null, true);
        echo "\">
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <div class=\"row justify-content-center mt-4\">
                                                <textarea class=\"messageInput form-control col-lg-11\" placeholder=\"Message\" id=\"exampleFormControlTextarea1\" rows=\"3\"></textarea>
                                                <button type=\"button\" class=\"mt-4 btn btn-lg col-lg-10 col-sm-6 col-md-6 col-6\">";
        // line 611
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Send Message"), "html", null, true);
        echo "</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class=\"infoContact col-lg-2 mt-5 text-left mb-5\" style=\"\">
                            <p class=\"mt-5 mb-2\">";
        // line 620
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("+33633495607"), "html", null, true);
        echo "</p>
                            <p class=\"mb-2\">PRO@ELIELALOUM.FR</p>
                            <p class=\"mb-2\">PERSO@ELIELALOUM.FR</p>
                            <p class=\"mb-5\">PARIS</p>
                        </div>

                        <div class=\"col-lg-1\"></div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!-- Footer -->
    <footer>
        <div class=\"container\">
            <ul class=\"list-inline\">
                <li class=\"list-inline-item\">
                    <a href=\"#\">";
        // line 643
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Home"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#about\">";
        // line 647
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("About"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#services\">";
        // line 651
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Services"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#services\">";
        // line 655
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Works"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#contact\">";
        // line 659
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Contact"), "html", null, true);
        echo "</a>
                </li>
                <li class=\"footer-menu-divider list-inline-item\">&sdot;</li>
                <li class=\"list-inline-item\">
                    <a href=\"#\">Back-Office</a>
                </li>
            </ul>
            <p class=\"copyright text-muted small\">Copyright &copy; Elie Laloum 2017. All Rights Reserved</p>
        </div>
    </footer>

";
    }

    public function getTemplateName()
    {
        return "EZPortfolioBundle:Portfolio:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  887 => 659,  880 => 655,  873 => 651,  866 => 647,  859 => 643,  833 => 620,  821 => 611,  812 => 605,  807 => 603,  792 => 591,  746 => 548,  742 => 547,  738 => 546,  698 => 509,  694 => 508,  690 => 507,  650 => 470,  646 => 469,  605 => 431,  601 => 430,  597 => 429,  555 => 390,  551 => 389,  547 => 388,  505 => 349,  500 => 347,  426 => 276,  403 => 256,  399 => 255,  393 => 252,  385 => 247,  381 => 246,  375 => 243,  359 => 230,  355 => 228,  351 => 227,  343 => 222,  337 => 219,  321 => 206,  317 => 205,  312 => 203,  304 => 198,  298 => 195,  287 => 187,  283 => 186,  279 => 185,  270 => 179,  249 => 161,  245 => 160,  229 => 147,  212 => 133,  197 => 121,  177 => 104,  167 => 97,  157 => 90,  147 => 83,  128 => 67,  122 => 64,  118 => 63,  114 => 62,  89 => 40,  85 => 39,  81 => 38,  77 => 37,  57 => 20,  47 => 12,  44 => 11,  36 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "EZPortfolioBundle:Portfolio:index.html.twig", "C:\\wamp64\\www\\EZone\\src\\EZ\\PortfolioBundle/Resources/views/Portfolio/index.html.twig");
    }
}
