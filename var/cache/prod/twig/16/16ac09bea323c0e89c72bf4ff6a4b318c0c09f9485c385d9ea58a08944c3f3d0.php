<?php

/* @EZPortfolio/Portfolio/portfolio_view.html.twig */
class __TwigTemplate_3b4364cbfcc53df28e19a1741ee06349a2030d0f5912218a096a8614aa142de3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EZPortfolioBundle::Layout/portfolio_layout.html.twig", "@EZPortfolio/Portfolio/portfolio_view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'ezportfolio_body' => array($this, 'block_ezportfolio_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EZPortfolioBundle::Layout/portfolio_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "
    <title>
        ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo " - ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
    </title>

";
    }

    // line 13
    public function block_ezportfolio_body($context, array $blocks = array())
    {
        // line 14
        echo "
    <iframe src=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["src"]) ? $context["src"] : null), "html", null, true);
        echo "\" style=\"width: 100%; height: 100vh;\"></iframe>

";
    }

    public function getTemplateName()
    {
        return "@EZPortfolio/Portfolio/portfolio_view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 15,  49 => 14,  46 => 13,  36 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@EZPortfolio/Portfolio/portfolio_view.html.twig", "C:\\wamp64\\www\\EZone\\src\\EZ\\PortfolioBundle\\Resources\\views\\Portfolio\\portfolio_view.html.twig");
    }
}
