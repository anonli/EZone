var imgAbout = jQuery('#about');

jQuery(window).on('scroll', function(){
    var winT = jQuery(window).scrollTop(),
        winH = jQuery(window).height(),
        imgT = imgAbout.offset().top;
    if(winT + winH  > imgT){
        $( ".img-about" ).fadeIn(2000);

    }
});

var skillsDiv = jQuery('#services');

jQuery(window).on('scroll', function(){
    var winT = jQuery(window).scrollTop(),
        winH = jQuery(window).height(),
        skillsT = skillsDiv.offset().top;
    if(winT + winH  > skillsT){
        $( "#st1" ).slideDown(2000).fadeIn( 700 );
        $( "#st2" ).fadeIn(2000);
        $( "#st3" ).slideDown(2000).fadeIn( 700 );
    }
});

function showTag (toGlock){
    $( "."+toGlock ).show(2000);
}
function hideTag (toGone) {
    $( "."+toGone ).hide(500);
}
